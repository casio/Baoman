package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ldy on 2015/3/14.
 */
public class DownBooks {
    private String date;// "2012-04-06",  日期
    private String icon;// "http://baozouzip.b0.upaiyun.com/icon19/001.png",   图书图片
    private String name;// "001",
    private String size;// "3.0M",   图书大小
    private String uuid;// "100001",
    private String zip_name;// "001.zip",
    private String zip_url;// "http://baozouzip.b0.upaiyun.com/19/001.zip",
    private String unzip_name;// "001"
    private int imgSwitch;

    public int getImgSwitch() {
        return imgSwitch;
    }

    public void setImgSwitch(int imgSwitch) {
        this.imgSwitch = imgSwitch;
    }

    //解析Json数据
    public void parseJSON(JSONObject json) {
        if (json != null) {
            try {
                date = json.getString("date");
                icon = json.optString("icon");
                name = json.getString("name");
                size = json.getString("size");
                uuid = json.getString("uuid");
                zip_name = json.getString("zip_name");
                zip_url = json.getString("zip_url");
                unzip_name = json.getString("unzip_name");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getZip_name() {
        return zip_name;
    }

    public void setZip_name(String zip_name) {
        this.zip_name = zip_name;
    }

    public String getZip_url() {
        return zip_url;
    }

    public void setZip_url(String zip_url) {
        this.zip_url = zip_url;
    }

    public String getUnzip_name() {
        return unzip_name;
    }

    public void setUnzip_name(String unzip_name) {
        this.unzip_name = unzip_name;
    }
}
