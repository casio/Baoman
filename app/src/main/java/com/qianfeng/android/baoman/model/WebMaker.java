package com.qianfeng.android.baoman.model;



import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ldy on 2015/3/12.
 */
public class WebMaker {
    private String id;// 11,
    private String icon;// "http://baozoumanhua.com/cloud_proxy/makers/credit.png",
    private String url;// "http://m.baozoumanhua.com/zhuangbi/credit_bill",
    private String name;// "信用卡年度账单",
    private String desc;// "又到了一年一度的败家大比拼",
    private String share_icon;// "http://baozoumanhua.com/cloud_proxy/makers/credit.png"


    //解析Json数据
    public void parseJSON(JSONObject json){
        if (json != null) {
            try {
                id = json.getString("id");
                icon = json.getString("icon");
                url = json.getString("url");
                name = json.getString("name");
                desc = json.getString("desc");
                share_icon = json.getString("share_icon");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getShare_icon() {
        return share_icon;
    }

    public void setShare_icon(String share_icon) {
        this.share_icon = share_icon;
    }
}
