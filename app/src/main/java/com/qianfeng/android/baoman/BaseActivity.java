package com.qianfeng.android.baoman;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.lidroid.xutils.DbUtils;

public class BaseActivity extends ActionBarActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private TextView txtTitle;
    private ImageButton btnBack;
    private ImageButton btnFun;
    private RadioGroup radioGroup;
    private DbUtils dbUtils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {

        super.setContentView(layoutResID);
        txtTitle = (TextView) findViewById(R.id.action_title);
        btnFun = (ImageButton) findViewById(R.id.action_function);
        btnBack = (ImageButton) findViewById(R.id.action_back);
        if (btnBack != null) {
            btnBack.setOnClickListener(this);
        }
        radioGroup = (RadioGroup) findViewById(R.id.bottomActionbar);

        dbUtils = DbUtils.create(getApplicationContext());
    }

    public DbUtils getDbUtils(){
        return dbUtils;
    }
    /**
     * 设置默认选中的RidioButton
     * @param i
     */
    public void setChildRadioChecked(int i){
        View view = radioGroup.getChildAt(i);
        if (view != null && view instanceof RadioButton) {
            RadioButton rb = (RadioButton) view;
//                设置RadioButton被点击
            rb.setChecked(true);
        }
        //设置默认的RadioButton点击事件
        radioGroup.setOnCheckedChangeListener(this);
    }

    /**
     * 设置回退按钮是否显示
     *
     * @param b
     */
    public void setBackVisible(boolean b) {
        if (b) {

            btnBack.setVisibility(View.VISIBLE);
        } else {
            btnBack.setVisibility(View.GONE);
        }
    }

    /**
     * 设置功能按钮是否显示
     *
     * @param b
     */
    public void setFunctionVisible(boolean b) {
        if (b) {
            btnFun.setVisibility(View.VISIBLE);
        } else {
            btnFun.setVisibility(View.GONE);
        }
    }

    /**
     * 设置标题
     *
     * @param str
     */
    public void setActionBarTitle(String str) {
        if (str != null) {
            txtTitle.setText(str);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.action_back) {
            finish();
        }
    }
    /**
     * RadioButton点击事件
     *
     * @param group
     * @param checkedId
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int count = group.getChildCount();
        int position = -1;
        for (int i = 0; i < count; i++) {
            View view = group.getChildAt(i);
            if (view instanceof RadioButton) {
                RadioButton rb = (RadioButton) view;
                if (rb.isChecked()) {
                    position = i;
                    break;
                }
            }
        }
        Intent intent=null;
        switch (position){
            case 0:
                intent=new Intent(this,TopicActivity.class);
                break;
            case 1:
                intent=new Intent(this,MovieActivity.class);
                break;
            case 2:
                intent=new Intent(this,FindActivity.class);
                break;
            case 3:
                intent=new Intent(this,PagerActivity.class);
                break;
            case 4:
                intent=new Intent(this,UserActivity.class);
                break;

        }
        if (intent != null) {
            startActivity(intent);
            overridePendingTransition(R.anim.anim_defult, R.anim.anim_defult);
            finish();
        }
    }
}