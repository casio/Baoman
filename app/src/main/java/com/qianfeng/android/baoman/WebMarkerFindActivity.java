package com.qianfeng.android.baoman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qianfeng.android.baoman.adapter.FindWebMarkerAdapter;
import com.qianfeng.android.baoman.model.WebMaker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class WebMarkerFindActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private FindWebMarkerAdapter adapter;
    private LinkedList<WebMaker> webMakers;

    final private String url = "http://api.ibaozou.com/api/v2/new_makers?client_id=10230158&sign=3f2878797dac61ca84bef39c8d4cfbff&timestamp=1426096187";

    //网络加载失败提示信息
    private LinearLayout load_layout;

    private ArrayList<String> urlList;
    private List<String> nameList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_web_marker_find);

        ImageView backImg = (ImageView) findViewById(R.id.find_web_marker_top_back);
        backImg.setOnClickListener(this);

        //加载数据中。
        load_layout = (LinearLayout) findViewById(R.id.load_layout);

        webMakers = new LinkedList<WebMaker>();
        adapter = new FindWebMarkerAdapter(this, webMakers);
        ListView webMarkerListView = (ListView) findViewById(R.id.web_marker_list_view);



        if (adapter != null) {

            webMarkerListView.setAdapter(adapter);

            getHttpDate(url);

        }

        webMarkerListView.setOnItemClickListener(this);

    }


    //获取网络数据
    private void getHttpDate(String url) {

        if (webMakers.size() == 0){
            load_layout.setVisibility(View.VISIBLE);
        }

        HttpUtils httpUtils = new HttpUtils();

        //存放Url
        urlList = new ArrayList<String>();
        nameList = new LinkedList<String>();

        httpUtils.send(
                HttpRequest.HttpMethod.GET,
                url,
                new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> objectResponseInfo) {
                        String result = objectResponseInfo.result;

                        List<WebMaker> webMakerList = new LinkedList<WebMaker>();
                        try {

                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray makers = jsonObject.getJSONArray("makers");
                            for (int i = 0; i < makers.length(); i++) {
                                JSONObject makersJSONObject = makers.getJSONObject(i);
                                WebMaker webMaker = new WebMaker();
                                webMaker.parseJSON(makersJSONObject);

                                String url = webMaker.getUrl();
                                urlList.add(url);

                                String name = webMaker.getName();
                                nameList.add(name);

                                webMakerList.add(webMaker);
                            }

                            webMakers.addAll(webMakerList);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        load_layout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(WebMarkerFindActivity.this, "加载失败,网络问题", Toast.LENGTH_SHORT).show();
                        load_layout.setVisibility(View.GONE);
                    }
                }
        );
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.find_web_marker_top_back:
                finish();
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(WebMarkerFindActivity.this,WebViewActivity.class);
        String url = urlList.get(position);
        String name = nameList.get(position);

        intent.putExtra("name",name);
        intent.putExtra("url",url);
        intent.putExtra("position",position);
        startActivity(intent);

    }
}
