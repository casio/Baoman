package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ldy on 2015/3/11.
 */
public class CartoonLatestInfo implements Serializable{
    /*"latest_info":{   //最新信息

                "id":174048,
                "name":"第4部12",
                "download_url":"http://hahazip.qiniudn.com/v2174048a888871268888a28395.zip"   //下载地址
    },*/

    private String id;
    private String name;
    private String downLoadUrl;

    //解析Json数据
    public void parseJSON(JSONObject json) {
        if (json != null) {
            try {
                id = json.getString("id");
                name = json.getString("name");
                downLoadUrl = json.getString("download_url");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownLoadUrl() {
        return downLoadUrl;
    }

    public void setDownLoadUrl(String downLoadUrl) {
        this.downLoadUrl = downLoadUrl;
    }


}
