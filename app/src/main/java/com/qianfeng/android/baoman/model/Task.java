package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ldy on 2015/3/10.
 */
    /*{
                "stage":{
                        "name":"顶你喜欢的10篇神作",    //任务名称
                        "award":"10",
                        "count":"10",
                        "id":0,
                        "receive_status":0,
                        "current_count":0,
                        "goto":"home"
                         },
                "id":1,
                "name":"顶你喜欢的10篇神作",//任务名称
                "desc":"狠狠的赞一下你喜欢的神作吧",  //任务描述
                "icon":"http://yun.baozoumanhua.com/reward/like_on.png",   //图片（带红色圆圈）
                "action":"up_article",
                "recommend":false,
                "end_icon":"http://yun.baozoumanhua.com/reward/like_off.png", //图片（不带红色圆圈）
                "perid":1
   */
public class Task {
    private String id;  //1,
    private String name;  //"顶你喜欢的10篇神作",//任务名称
    private String desc;  //"狠狠的赞一下你喜欢的神作吧",  //任务描述
    private String icon;  //"http://yun.baozoumanhua.com/reward/like_on.png",   //图片（带红色圆圈）
    private String action;  //"up_article",
    private String recommend;  //false,
    private String end_icon;  //"http://yun.baozoumanhua.com/reward/like_off.png", //图片（不带红色圆圈）
    private String perid;  //1

    private Stage stage;


    //解析Json数据
    public void parseJSON(JSONObject json){
        if (json != null) {
            try {

                id = json.getString("id");
                name = json.getString("name");
                desc = json.getString("desc");
                icon = json.getString("icon");
                action = json.getString("action");
                recommend = json.getString("recommend");
                end_icon = json.getString("end_icon");
                perid = json.getString("perid");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public String getEnd_icon() {
        return end_icon;
    }

    public void setEnd_icon(String end_icon) {
        this.end_icon = end_icon;
    }

    public String getPerid() {
        return perid;
    }

    public void setPerid(String perid) {
        this.perid = perid;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
