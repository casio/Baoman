package com.qianfeng.android.baoman.fragment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.TopicPagerMovieActivity;
import com.qianfeng.android.baoman.TopicPicturesDetailActivity;
import com.qianfeng.android.baoman.model.Jokable;
import com.qianfeng.android.baoman.model.JokePoint;
import com.qianfeng.android.baoman.view.MyImage;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopicPagerFragment extends Fragment implements View.OnClickListener {


    private String mp4_file_link;
    private String pictures;

    public TopicPagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View ret = inflater.inflate(R.layout.fragment_topic_pager, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            JokePoint jokePoint = (JokePoint) bundle.getSerializable("joke_point");
            String icon = jokePoint.getIcon();
            String title = jokePoint.getTitle();

            Jokable jokable = (Jokable) bundle.getSerializable("jokable");
            pictures = jokable.getPictures();
            mp4_file_link = jokable.getMp4_file_link();
            TextView textView = (TextView) ret.findViewById(R.id.topic_pager_title);
            textView.setText(title);
            MyImage myImage = (MyImage) ret.findViewById(R.id.topic_pager_ico);
            myImage.setOnClickListener(this);
            BitmapHelp.getUtils().display(myImage, icon, new BitmapLoadCallBack<MyImage>() {
                @Override
                public void onLoadCompleted(MyImage myImage, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {

                    myImage.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadFailed(MyImage myImage, String s, Drawable drawable) {

                }
            });
        }
        return ret;
    }


    @Override
    public void onClick(View v) {

        if (!mp4_file_link.equals("") && pictures.equals("")) {
            Intent intent = new Intent(getActivity(), TopicPagerMovieActivity.class);
            intent.putExtra("mp4_file_link", mp4_file_link);
            getActivity().startActivity(intent);
        } else if (mp4_file_link.equals("") && !pictures.equals("")) {
            Intent intent = new Intent(getActivity(), TopicPicturesDetailActivity.class);
            intent.putExtra("pictures", pictures);
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void onDestroy() {


        Log.e("TopicPagerFragment","onDestroy");
        super.onDestroy();
    }
}
