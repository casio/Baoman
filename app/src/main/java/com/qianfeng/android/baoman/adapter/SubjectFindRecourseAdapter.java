package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.Subject;

import java.util.List;

/**
 * Created by ldy on 2015/3/12.
 */
public class    SubjectFindRecourseAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Subject> subjects;

    public SubjectFindRecourseAdapter(Context context, List<Subject> subjects) {

        if (context != null) {
            inflater = LayoutInflater.from(context);
        } else {
            throw new IllegalArgumentException("Context must be not null");
        }

        this.subjects = subjects;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if (subjects != null) {
            ret = subjects.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int i) {
        Object ret = null;
        if (subjects != null) {
            ret = subjects.get(i);
        }
        return ret;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;

        if (convertView != null) {

            ret = convertView;

        } else {

            ret = inflater.inflate(R.layout.item_subject_recourse_find, parent, false);

        }



        ViewHolder holder = (ViewHolder) ret.getTag();

        if (holder == null) {
            holder = new ViewHolder();
            holder.subjectImg = (ImageView) ret.findViewById(R.id.find_recourse_subject_img);
            holder.subjectTitle = (TextView) ret.findViewById(R.id.find_recourse_subject_title);

            ret.setTag(holder);
        }

        Subject subject = subjects.get(position);

        holder.subjectImg.setImageResource(R.drawable.maker_default_face_1);
        String cover_path = subject.getCover_path();
        holder.subjectImg.setTag(cover_path);


        String name = subject.getName();
        holder.subjectTitle.setText(name);


        BitmapHelp.getUtils().display(holder.subjectImg, subject.getCover_path(), new BitmapLoadCallBack<ImageView>() {
            @Override
            public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

            }

        });

        return ret;
    }


    /**
     * 避免每次进行findViewById
     */
    private static class ViewHolder {
        public ImageView subjectImg;
        public TextView subjectTitle;
    }
}
