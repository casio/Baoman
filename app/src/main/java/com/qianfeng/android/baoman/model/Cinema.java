package com.qianfeng.android.baoman.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Android_Studio on 2015/3/10.
 * User:Maxiaoyu
 * Date:2015/3/10
 * Email:731436452@qq.com
 */
public class Cinema implements Serializable {
    private int id;// 1745,
    private String name;// "每日一暴",
    private String description;// "爆一爆呐个爆一爆，爆得那个节操往下掉~\r\n",
    private int score;// 0,
    private int public_articles_pos;// 4198400,
    private int public_comments_count;// 138578,
    private String type;// "Series",
    private String pictures;// "http://wanzao2.b0.upaiyun.com/system/series/icons/1745/original/1745.jpg",
    private String small_pictures;// "http://wanzao2.b0.upaiyun.com/system/series/icons/1745/original/1745.jpg-s1",
    private int total_articles_count;// 229,
    private String created_at;// "2014-06-16 11:49:15",
    //            "articles":
    private List<CinemaItem> cinemaItems;

    public Cinema() {
        cinemaItems = new LinkedList<>();
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }

    @Override
    public boolean equals(Object o) {
        Cinema cinema = null;
        if (o instanceof Cinema)
            cinema = (Cinema) o;
        return this.id == cinema.id;
    }

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                id = jsonObject.getInt("id");//
                name = jsonObject.getString("name");
                description = jsonObject.getString("description");//
                score = jsonObject.getInt("score");//
                public_articles_pos = jsonObject.getInt("public_articles_pos");//
                public_comments_count = jsonObject.getInt("public_comments_count");//
                type = jsonObject.getString("type");//
                pictures = jsonObject.getString("pictures");//
                small_pictures = jsonObject.getString("small_pictures");//
                total_articles_count = jsonObject.getInt("total_articles_count");//
                created_at = jsonObject.getString("created_at");//
                JSONArray articles = jsonObject.getJSONArray("articles");
                for (int i = 0; i < articles.length(); i++) {
                    JSONObject object = articles.getJSONObject(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getPublic_articles_pos() {
        return public_articles_pos;
    }

    public void setPublic_articles_pos(int public_articles_pos) {
        this.public_articles_pos = public_articles_pos;
    }

    public int getPublic_comments_count() {
        return public_comments_count;
    }

    public void setPublic_comments_count(int public_comments_count) {
        this.public_comments_count = public_comments_count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public String getSmall_pictures() {
        return small_pictures;
    }

    public void setSmall_pictures(String small_pictures) {
        this.small_pictures = small_pictures;
    }

    public int getTotal_articles_count() {
        return total_articles_count;
    }

    public void setTotal_articles_count(int total_articles_count) {
        this.total_articles_count = total_articles_count;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<CinemaItem> getCinemaItems() {
        return cinemaItems;
    }

    public void setCinemaItems(List<CinemaItem> cinemaItems) {
        this.cinemaItems = cinemaItems;
    }
}
