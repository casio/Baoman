package com.qianfeng.android.baoman;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;


public class SplashActivity extends ActionBarActivity implements Animation.AnimationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //1.通过代码去掉标题栏,为什么这里这么写会报错？
//        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        WindowManager m = getWindowManager();
        Display d = m.getDefaultDisplay();
        int width = d.getWidth();
        int height = d.getHeight();

        ImageView img = (ImageView) findViewById(R.id.splash_img);


        ScaleAnimation scaleAnimation = new ScaleAnimation(1, 1.1f, 1, 1.1f, width / 2, height / 2);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(2000);
        scaleAnimation.setRepeatMode(Animation.RELATIVE_TO_SELF);//还不知道那个参数是让图片放大后不会恢复
        scaleAnimation.setAnimationListener(this);
        img.startAnimation(scaleAnimation);
//        ObjectAnimator animator1 = ObjectAnimator.ofFloat(img, "scale", 1.0F, 0.5F).setDuration(2000);
//        ObjectAnimator animator2 = ObjectAnimator.ofFloat(ico, "translationX", ico.getX(), width).setDuration(2000);
//        animator2.addListener(this);
//        AnimatorSet animatorSet = new AnimatorSet();
//        animatorSet.playSequentially(animator1, animator2);
//        animatorSet.start();

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Intent intent = new Intent(this, TopicActivity.class);
        intent.putExtra("firstLoad",true);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_defult, R.anim.anim_defult);
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


//    @Override
//    public void onAnimationStart(Animator animation) {
//
//    }
//
//    @Override
//    public void onAnimationEnd(Animator animation) {
//        startActivity(new Intent(this, MainActivity.class));
//        finish();
//    }
//
//    @Override
//    public void onAnimationCancel(Animator animation) {
//
//    }
//
//    @Override
//    public void onAnimationRepeat(Animator animation) {
//
//    }
}
