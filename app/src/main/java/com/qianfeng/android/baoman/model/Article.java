package com.qianfeng.android.baoman.model;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhangkx on 2015/3/10.
 */
@Table(name = "article")
public class Article {

    /*
            "alt_score": 91436,
            "comment_status": "",
            "content": "",
            "created_at": "2015-03-10 01:15:55",
            "face_id": "",
            "for_mobile": true,
            "group_id": 19,
            "id": 17301776,
            "neg": -177,
            "original_id": "",
            "pos": 11898,
            "published_at": "2015-03-10 01:16:13",
            "score": 11721,
            "series_id": null,
            "status": "publish",
            "title": "王尼玛和曹尼玛77",
            "updated_at": "2015-03-10T11:30:51+08:00",
            "user_id": 530806,
            "pictures": "http://ww3.sinaimg.cn/large/005XOoDPjw1eq0dxp1i3oj30cs26m445.jpg",
            "height": 2830,
            "width": 460,
            "small_pictures": "http://wanzao2.b0.upaiyun.com/system/avatars/530806/original/20130801170814321.jpg-s1",
            "series": {},
            "user": {
                "login": "宇宙人z",
                "avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/530806/original/20130801170814321.jpg-s1",
                "id": 530806
            },
            "user_login": "宇宙人z",
            "user_avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/530806/original/20130801170814321.jpg-s1",
            "picture_type": 1,
            "poll_type": 0,
            "poll": {},
            "reward_count": 0,
            "public_comments_count": 273,
            "watched_count": 93,
            "group": {
                "name": "暴走漫画"
            },
            "ad_url_ios": "",
            "ad_url_android": "",
            "ad_url_winphone": "",
            "ad_url_web": "",
            "watched": false
     */

    public Article() {
    }

    @Transient
    private String alt_score;//"alt_score": 91436,
    @Transient
    private String comment_status;//"comment_status": "",
    @Transient
    private String content;//"content": "",
    @Transient
    private String created_at;//"created_at": "2015-03-10 01:15:55",
    @Transient
    private String face_id;//"face_id": "",
    //    private String for_mobile;//"for_mobile": true,
    @Transient
    private String group_id;//"group_id": 19,
    @Transient
    private String id;//"id": 17301776,
    @Column(column = "neg")
    private String neg;//"neg": -177,
    @Transient
    private String original_id;//"original_id": "",
    @Column(column = "pos")
    private String pos;//"pos": 11898,
    @Column(column = "published_at")
    private String published_at;//"published_at": "2015-03-10 01:16:13",
    @Transient
    private String score;//"score": 11721,
    @Transient
    private String series_id;//"series_id": null,
    @Transient
    private String status;//"status": "publish",
    @Column(column = "title")
    private String title;//"title": "王尼玛和曹尼玛77",
    @Transient
    private String updated_at;//"updated_at": "2015-03-10T11:30:51+08:00",
    @Transient
    private String user_id;//"user_id": 530806,
    @Column(column = "pictures")
    private String pictures;//"pictures": "http://ww3.sinaimg.cn/large/005XOoDPjw1eq0dxp1i3oj30cs26m445.jpg",
    @Transient
    private String height;//"height": 2830,
    @Transient
    private String width;//"width": 460,
    @Transient
    private String small_pictures;//"small_pictures": "http://wanzao2.b0.upaiyun.com/system/avatars/530806/original/20130801170814321.jpg-s1",
    @Transient
    private String series;//"series": {},
    @Column(column = "user_login")
    private String user_login;//"user_login": "宇宙人z",
    @Transient
    private String user_avatar;//"user_avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/530806/original/20130801170814321.jpg-s1",
    @Transient
    private String picture_type;//"picture_type": 1,
    @Transient
    private String poll_type;//"poll_type": 0,
    @Transient
    private String poll;//"poll": {},
    @Transient
    private String reward_count;//"reward_count": 0,
    @Column(column = "public_comments_count")
    private String public_comments_count;//"public_comments_count": 273,
    @Transient
    private String watched_count;//"watched_count": 93,
    @Transient
    private String ad_url_ios;//"ad_url_ios": "",
    @Transient
    private String ad_url_android;//"ad_url_android": "",
    @Transient
    private String ad_url_winphone;//"ad_url_winphone": "",
    @Transient
    private String ad_url_web;//"ad_url_web": "",
    //    private String watched;//"watched": false
    @Transient
    private User user;
    @Transient
    private Group group;
    @Column(column = "mp4_file_link")
    private String mp4_file_link;   //"ad_url_winphone": "",
    @Column(column = "mp4_image_link")
    private String mp4_image_link;   //"ad_url_web": ""
    @Transient
    private String thumb;   //"thumb": "http://wanzao2.b0.upaiyun.com/system/pictures/17355071/original/8446263b0bfdbfa9.png-bzthumb",


    @Column(column = "enjoy")
    private boolean enjoy;

    public boolean isEnjoy() {
        return enjoy;
    }

    public void setEnjoy(boolean enjoy) {
        this.enjoy = enjoy;
    }

    /**
     * 解析JSON数据
     *
     * @param json
     */
    public void parseJSON(JSONObject json) {
        if (json != null) {
            try {
                alt_score = json.getString("alt_score"); // 必须存在的字段优先解析，这样保证数据的有效性
                created_at = json.getString("created_at");
                group_id = json.getString("group_id");
                id = json.getString("id");
                published_at = json.getString("published_at");
                score = json.getString("score");
                status = json.getString("status");
                title = json.getString("title");
                updated_at = json.getString("updated_at");
                user_id = json.getString("user_id");

                small_pictures = json.getString("small_pictures");
                user_login = json.getString("user_login");
                user_avatar = json.getString("user_avatar");
                public_comments_count = json.getString("public_comments_count");
                watched_count = json.getString("watched_count");


                pictures = json.optString("pictures");
                thumb = json.optString("thumb");
                height = json.optString("height");
                width = json.optString("width");
                mp4_file_link = json.optString("mp4_file_link");
                mp4_image_link = json.optString("mp4_image_link");
                content = json.optString("content");
                face_id = json.optString("face_id");
                neg = json.optString("neg");
                original_id = json.optString("original_id");
                pos = json.optString("pos");
                series_id = json.optString("series_id");
                series = json.optString("series");
                picture_type = json.optString("picture_type");
                poll_type = json.optString("poll_type");
                poll = json.optString("poll");
                reward_count = json.optString("reward_count");
                comment_status = json.optString("comment_status");
                ad_url_ios = json.optString("ad_url_ios");
                ad_url_android = json.optString("ad_url_android");
                ad_url_winphone = json.optString("ad_url_winphone");
                ad_url_web = json.optString("ad_url_web");


                JSONObject userJson = json.getJSONObject("user");
                user = new User();
                user.parseJSON(userJson);

                JSONObject groupJson = json.getJSONObject("group");
                group = new Group();
                group.parseJSON(groupJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getAlt_score() {
        return alt_score;
    }

    public String getComment_status() {
        return comment_status;
    }

    public String getContent() {
        return content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getFace_id() {
        return face_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public String getId() {
        return id;
    }

    public String getNeg() {
        return neg;
    }

    public String getOriginal_id() {
        return original_id;
    }

    public String getPos() {
        return pos;
    }

    public String getPublished_at() {
        return published_at;
    }

    public String getScore() {
        return score;
    }

    public String getSeries_id() {
        return series_id;
    }

    public String getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getPictures() {
        return pictures;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getSmall_pictures() {
        return small_pictures;
    }

    public String getSeries() {
        return series;
    }

    public String getUser_login() {
        return user_login;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public String getPicture_type() {
        return picture_type;
    }

    public String getPoll_type() {
        return poll_type;
    }

    public String getPoll() {
        return poll;
    }

    public String getReward_count() {
        return reward_count;
    }

    public String getPublic_comments_count() {
        return public_comments_count;
    }

    public String getWatched_count() {
        return watched_count;
    }

    public String getAd_url_ios() {
        return ad_url_ios;
    }

    public String getAd_url_android() {
        return ad_url_android;
    }

    public String getAd_url_winphone() {
        return ad_url_winphone;
    }

    public String getAd_url_web() {
        return ad_url_web;
    }

    public User getUser() {
        return user;
    }

    public Group getGroup() {
        return group;
    }

    public String getMp4_file_link() {
        return mp4_file_link;
    }

    public String getMp4_image_link() {
        return mp4_image_link;
    }

    public String getThumb() {
        return thumb;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public void setNeg(String neg) {
        this.neg = neg;
    }
}
