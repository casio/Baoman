package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.core.BitmapSize;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.Cinema;

import java.util.List;

/**
 * Created by Android_Studio on 2015/3/10.
 * User:Maxiaoyu
 * Date:2015/3/10
 * Email:731436452@qq.com
 */
public class MovieAdapter extends BaseAdapter {

    private List<Cinema> data;
    private LayoutInflater inflater;

    public MovieAdapter(List<Cinema> data, Context context) {
        this.data = data;
        if (context != null) {
            inflater = LayoutInflater.from(context);
        }


    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;
        if (convertView != null) {
            ret = convertView;
        } else {
            ret = inflater.inflate(R.layout.item_movie_index, parent, false);
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.image = (ImageView) ret.findViewById(R.id.movie_index_image);
            holder.title = (TextView) ret.findViewById(R.id.movie_index_title);
            holder.desc = (TextView) ret.findViewById(R.id.movie_index_desc);
            holder.ding = (TextView) ret.findViewById(R.id.movie_index_ding);
            holder.comment = (TextView) ret.findViewById(R.id.movie_index_comment);
            holder.count = (TextView) ret.findViewById(R.id.movie_index_count);
            ret.setTag(holder);
        }
        Cinema cinema = data.get(position);
        holder.title.setText(cinema.getName());
        holder.desc.setText(cinema.getDescription());
        holder.ding.setText(Integer.toString(cinema.getPublic_articles_pos()));
        holder.comment.setText(Integer.toString(cinema.getPublic_comments_count()));
        holder.count.setText("更新到" + cinema.getTotal_articles_count() + "话");

        String small_pictures = cinema.getSmall_pictures();
        if (cinema.getPictures() != null) {
//            ImageHelper.setImageIcon(small_pictures, holder.image, 70, 100);

            BitmapDisplayConfig config = new BitmapDisplayConfig();
//            config.setShowOriginal(true); // 显示原始图片,不压缩, 尽量不要使用,
            // 图片太大时容易BOOM。
            config.setBitmapConfig(Bitmap.Config.RGB_565);
            config.setBitmapMaxSize(new BitmapSize(84, 108));

            BitmapHelp.getUtils().display(holder.image, cinema.getPictures(), config);


        }

        return ret;
    }

    public static class ViewHolder {
        public ImageView image;
        public TextView title;
        public TextView desc;
        public TextView ding;
        public TextView comment;
        public TextView count;
    }
}
