package com.qianfeng.android.baoman;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;

import org.json.JSONException;
import org.json.JSONObject;


public class RegisterActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setActionBarTitle("注册暴走");
        setBackVisible(true);
        setFunctionVisible(true);
        ImageButton fun = (ImageButton) findViewById(R.id.action_function);
        fun.setImageResource(R.drawable.register_btn_selector);
        fun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(Constants.REGISTER_URL);
            }
        });

    }

    private void loadData(String url) {
        HttpUtils httpUtils = new HttpUtils();
        RequestParams params = new RequestParams();
        params.addBodyParameter("user[login]", "192272753");
        params.addBodyParameter("user[password_confirmation]", "1234567987");
        params.addBodyParameter("user[email]", "192272753@qq.com");
        params.addBodyParameter("user[password]", "1234567987");
        httpUtils.send(
                HttpRequest.HttpMethod.POST,
                url,
                params, new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> objectResponseInfo) {
                        String result = objectResponseInfo.result;
                        if (result != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                String user_name = jsonObject.getString("user_name");
                                String user_id = jsonObject.getString("user_id");
                                String access_token = jsonObject.getString("access_token");
                                String avatar = jsonObject.getString("avatar");
                                Constants instance = Constants.getInstance();
                                instance.save("user_name", user_name);
                                instance.save("user_id", user_id);
                                instance.save("access_token", access_token);
                                instance.save("avatar", avatar);
                                instance.save("lastTime", Long.toString(System.currentTimeMillis()));
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(intent);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {

                    }
                });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }
}
