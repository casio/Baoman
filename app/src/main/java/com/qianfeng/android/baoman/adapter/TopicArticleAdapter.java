package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.lidroid.xutils.exception.DbException;
import com.qianfeng.android.baoman.BaseActivity;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.TopicPagerMovieActivity;
import com.qianfeng.android.baoman.model.Article;

import java.util.List;

import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

/**
 * Created by zhangkx on 2015/3/10.
 */
public class TopicArticleAdapter extends BaseAdapter implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private List<Article> articles;
    private LayoutInflater inflater;
    private Context context;
    private DbUtils dbUtils;

    public TopicArticleAdapter(Context context, List<Article> articles) {
        if (context != null) {
            inflater = LayoutInflater.from(context);
            this.context = context;
        } else {
            throw new IllegalArgumentException("Context must not all");
        }
        this.articles = articles;
        BaseActivity baseActivity = (BaseActivity) context;
    }


    @Override
    public int getCount() {
        int ret = 0;
        if (articles != null) {
            ret = articles.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View ret = null;

        if (convertView != null) {

            ret = convertView;

        } else {

            ret = inflater.inflate(R.layout.item_topic_article, parent, false);


        }


        ViewHolder holder = (ViewHolder) ret.getTag();

        if (holder == null) {
            holder = new ViewHolder();
            holder.imgIcon = (ImageView) ret.findViewById(R.id.item_article_img);
            holder.txtUsername = (TextView) ret.findViewById(R.id.item_user_name);
            holder.txtTitle = (TextView) ret.findViewById(R.id.item_article_title);
            holder.imgAvatar = (ImageView) ret.findViewById(R.id.item_user_avatar);
            holder.txtTime = (TextView) ret.findViewById(R.id.item_time);
            holder.imgBtn = (ImageButton) ret.findViewById(R.id.item_article_video_btn);
            holder.cbPos = (CheckBox) ret.findViewById(R.id.item_article_pos);
            holder.cbNeg = (CheckBox) ret.findViewById(R.id.item_article_neg);
            holder.cbCommentsCount = (CheckBox) ret.findViewById(R.id.item_article_public_comments_count);
            holder.cbShare = (CheckBox) ret.findViewById(R.id.item_article_share);
            holder.cbMark = (CheckBox) ret.findViewById(R.id.item_article_enjoy);
            holder.cProgressbar = (ProgressBar) ret.findViewById(R.id.item_article_progress);

            ret.setTag(holder);
        }

        holder.cbPos.setTag(position);
        holder.cbNeg.setTag(position);
        holder.cbShare.setTag(position);
        holder.cbMark.setTag(position);


        holder.imgBtn.setTag(position);

        Article article = articles.get(position);

        String s = article.getTitle();

        holder.txtTitle.setText(s);

        s = article.getUser_login();
        holder.txtUsername.setText(s);

        s = article.getPublished_at();
        holder.txtTime.setText(s);

        s = article.getPos();
        holder.cbPos.setText(s);
        holder.cbPos.setOnCheckedChangeListener(this);

        s = article.getNeg();
        holder.cbNeg.setText(String.valueOf(-Integer.parseInt(s)));
        holder.cbNeg.setOnCheckedChangeListener(this);

        holder.cbShare.setOnCheckedChangeListener(this);
        holder.cbMark.setChecked(article.isEnjoy());
        holder.cbMark.setOnCheckedChangeListener(this);
        s = article.getPublic_comments_count();
        holder.cbCommentsCount.setText(s);
        //判断是视频还是图片
        if (!article.getPictures().equals("")) {
            s = article.getPictures();
            holder.imgBtn.setVisibility(View.GONE);
        } else {
            s = article.getMp4_image_link();
            holder.imgBtn.setVisibility(View.VISIBLE);
//            String mp4_file_link = article.getMp4_file_link();
//            holder.imgBtn.setTag(mp4_file_link);
            holder.imgBtn.setOnClickListener(this);

        }

        final ProgressBar progressBar = holder.cProgressbar;
        BitmapHelp.getUtils().display(holder.imgIcon, s, new BitmapLoadCallBack<ImageView>() {

            @Override
            public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                progressBar.setVisibility(View.GONE);
                imageView.setImageBitmap(bitmap);

            }

            @Override
            public void onPreLoad(ImageView container, String uri, BitmapDisplayConfig config) {
                super.onPreLoad(container, uri, config);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadStarted(ImageView container, String uri, BitmapDisplayConfig config) {
                super.onLoadStarted(container, uri, config);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoading(ImageView container, String uri, BitmapDisplayConfig config, long total, long current) {
                super.onLoading(container, uri, config, total, current);

                progressBar.setProgress((int) (current / total) * 100);
            }

            @Override
            public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

            }
        });
        BitmapHelp.getUtils().display(holder.imgAvatar, article.getSmall_pictures(), new BitmapLoadCallBack<ImageView>() {
            @Override
            public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                imageView.setImageBitmap(bitmap);

            }

            @Override
            public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

            }
        });
        return ret;
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag instanceof Integer) {
            int p = (Integer) tag;
            Article article1 = articles.get(p);
            String mp4_file_link = article1.getMp4_file_link();
            Intent intent = new Intent(context, TopicPagerMovieActivity.class);
            intent.putExtra("mp4_file_link", mp4_file_link);
            context.startActivity(intent);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Object tag = buttonView.getTag();
        if (tag instanceof Integer) {
            int p = (Integer) tag;
            Article article1 = articles.get(p);

            if (buttonView.getId() == R.id.item_article_pos) {
                if (isChecked) {
                    article1.setPos(String.valueOf((Integer.parseInt(article1.getPos()) + 1)));

                } else {
                    article1.setPos(String.valueOf((Integer.parseInt(article1.getPos()) - 1)));
                }
                this.notifyDataSetChanged();
            } else if (buttonView.getId() == R.id.item_article_neg) {
                if (isChecked) {
                    article1.setNeg(String.valueOf((Integer.parseInt(article1.getNeg()) - 1)));
                } else {
                    article1.setNeg(String.valueOf((Integer.parseInt(article1.getNeg()) + 1)));
                }
                this.notifyDataSetChanged();
            } else if (buttonView.getId() == R.id.item_article_share) {
                if (isChecked) {
                    showShare();
                }
            } else if (buttonView.getId() == R.id.item_article_enjoy) {
                article1.setEnjoy(isChecked);
                iEnjoyListener.onEnjoy(article1, isChecked);
            }

        }
    }

    private IEnjoyListener iEnjoyListener;

    public void setiEnjoyListener(IEnjoyListener iEnjoyListener) {
        this.iEnjoyListener = iEnjoyListener;
    }

    public interface IEnjoyListener {
        public void onEnjoy(Article article, Boolean b);
    }

    /**
     * 避免每次进行findViewById
     */
    private static class ViewHolder {
        public TextView txtUsername;
        public TextView txtTitle;
        public TextView txtTime;
        public ImageView imgAvatar;
        public ImageView imgIcon;
        public ImageButton imgBtn;
        public CheckBox cbPos;
        public CheckBox cbNeg;
        public CheckBox cbCommentsCount;
        public CheckBox cbShare;
        //收藏
        public CheckBox cbMark;
        //进度条
        public ProgressBar cProgressbar;
    }

    private void showShare() {
        ShareSDK.initSDK(context);
        OnekeyShare oks = new OnekeyShare();
        // 分享时Notification的图标和文字
//        oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        oks.setText("我是分享文本");
        oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/05/21/oESpJ78_533x800.jpg");
        // 启动分享GUI
        oks.show(context);
    }
}
