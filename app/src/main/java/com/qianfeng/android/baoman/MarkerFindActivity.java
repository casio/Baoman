package com.qianfeng.android.baoman;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class MarkerFindActivity extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener {


    //存放图片的集合
    private List<Map<String, Object>> data;

    final Integer[] icons = new Integer[]{
            R.drawable.factory_baoman_btn_old_selector,
            R.drawable.baozouti_btn_selector,//2
            R.drawable.tucao_btn_selector,
            R.drawable.naocan_btn_selector,
            R.drawable.face_maker_btn_selector,//5
            R.drawable.chat_maker_btn_selector,//6

    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_find_marker_item);


        data = new LinkedList<Map<String, Object>>();
        HashMap<String, Object> map;

        for (int i = 0; i < icons.length; i++) {
            map = new HashMap<String, Object>();
            map.put("markerIcon", icons[i]);
            data.add(map);
        }


        final SimpleAdapter adapter = new SimpleAdapter(
                this,
                data,
                R.layout.item_marker_find,
                new String[]{"markerIcon"},
                new int[]{R.id.find_marker_item_icon}
        );

        GridView gridView = (GridView) findViewById(R.id.find_marker_fragment_grid_view);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(this);

        //返回键的处理
        ImageView backIcon = (ImageView) findViewById(R.id.find_marker_top_back);
        backIcon.setOnClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //TODO GridView条目点击事件的处理
    }



    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.find_marker_top_back){
            finish();
        }
    }
}
