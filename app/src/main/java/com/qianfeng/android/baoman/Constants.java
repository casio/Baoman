package com.qianfeng.android.baoman;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.qianfeng.android.baoman.model.Face;
import com.qianfeng.android.baoman.model.LoginUser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Android_Studio on 2015/3/10.
 * User:Maxiaoyu
 * Date:2015/3/10
 * Email:731436452@qq.com
 */
public class Constants {

    private static Constants constants;
    private SharedPreferences sp;
    private LoginUser user;


    private Context context;

    private Constants() {
    }

    public static Constants getInstance() {
        if (constants == null) {
            constants = new Constants();

        }
        return constants;
    }


    public static String MOVIE_INDEX_URL = "http://api.ibaozou.com/series/cinema.app?client_id=10230158&ignore_for_mobile=true&page=1&pagesize=10";

    public static List<Face> faceList = new ArrayList<Face>();
    public static String MOVIE_ALL_FACE = "http://api.ibaozou.com/api/v1/faces/all";

    public static String REGISTER_URL = "http://api.ibaozou.com/register.app?client_id=10230158";

    public static String LOGIN_URL = "http://api.ibaozou.com/api/v2/login";


    public void setContext(Context paramContext) {
        this.context = paramContext;
        if (paramContext != null) {
            this.sp = paramContext.getSharedPreferences("baoman", Context.MODE_PRIVATE);
        }
    }



    public static String getMovieIndexUrl(String clientId, int page, int pageSize) {

        return "http://api.ibaozou.com/series/cinema.app?" +
                "client_id=10230158" +
                "&ignore_for_mobile=true" +
                "&page=" + page +
                "&pagesize=" + pageSize;
    }

    //    http://api.ibaozou.com/series/1745/articles/page/1.app?client_id=10230158&ignore_for_mobile=true
    public static String getMovieSubList(String clientId, int movieId, int page) {
        return "http://api.ibaozou.com/series/" + movieId +
                "/articles/page/" + page +
                ".app?" +
                "client_id=10230158" +
                "&ignore_for_mobile=true";
    }

    //http://api.ibaozou.com/api/v1/articles/17356800/comments?client_id=10230158&page=1&order=desc&per_page=10
//http://api.ibaozou.com/api/v1/articles/17037175/comments?client_id=10230158&page=1&order=best&per_page=10
//http://api.ibaozou.com/api/v1/articles/17037175/comments?client_id=10230158&page=1&order=new&per_page=10
//http://api.ibaozou.com/api/v1/articles/17037175/comments?client_id=10230158&page=1&order=high&per_page=10
    public static String getMovieCommentURL(String clientId, int commentId, int page, String order, int per_page) {
        return "http://api.ibaozou.com/api/v1/articles/" + commentId +
                "/comments?client_id=10230158" +
                "&page=" + page +
                "&order=" + order +
                "&per_page=" + per_page;
    }

    //    http://api.ibaozou.com/users/3970212.app
    public static String getOthersDetail(String userId) {
        return "http://api.ibaozou.com/users/" + userId + ".app";
    }

    //    http://api.ibaozou.com/api/v2/users_redirect/3970212/articles/page/1.app?client_id=10230158&per_page=10
    public static String getOtherCreation(String userId, int page) {
        return "http://api.ibaozou.com/api/v2/users_redirect/" + userId +
                "/articles/page/1.app?" +
                "client_id=10230158" +
                "&per_page=" + page;
    }

    //    http://api.ibaozou.com/users/8900345.app?client_id=10230158&access_token=f2d25646406519dff5dea1ec510f207ba50dc06b&user_id=8900345
    public static String getLoginUserDetail(String client_id, String token, int userId) {
        return "http://api.ibaozou.com/users/8900345.app?" +
                "client_id=" + client_id +
                "&access_token=" + token +
                "&user_id=" + userId;
    }


    public static String get32MD5(String paramString) {
        int i = 0;
        char[] arrayOfChar1 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
        try {
            byte[] arrayOfByte1 = paramString.getBytes();
            MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
            localMessageDigest.update(arrayOfByte1);
            byte[] arrayOfByte2 = localMessageDigest.digest();
            int j = arrayOfByte2.length;
            char[] arrayOfChar2 = new char[j * 2];
            int k = 0;
            for (; ; ) {
                if (i >= j) {
                    return new String(arrayOfChar2);
                }
                int m = arrayOfByte2[i];
                int n = k + 1;
                arrayOfChar2[k] = arrayOfChar1[(0xF & m >> 4)];
                k = n + 1;
                arrayOfChar2[n] = arrayOfChar1[(m & 0xF)];
                i++;
            }
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return null;
    }

    public static String getMD5Sign(Map<String, String> paramMap, List<String> paramList) {
        StringBuilder localStringBuilder = new StringBuilder();
        if ((paramMap == null) || (paramList == null)) {
            return "";
        }
        Collections.sort(paramList);
        for (int i = 0; i < paramList.size(); i++) {
            if (paramMap.containsKey(paramList.get(i))) {
                String s = paramList.get(i);
                String s1 = paramMap.get(s);
                localStringBuilder.append(s + "=" + s1);
            }
        }
        localStringBuilder.append("18a75cf12dff8cf6e17550e25c860839");
        String str1 = localStringBuilder.toString();
        Log.i("Common", "sign = " + str1);
        String str2 = null;
        try {
            str2 = URLEncoder.encode(str1, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Constants.get32MD5(str2);
    }

    public LoginUser getUser() {
        if (this.user == null) {
            this.user = new LoginUser();
            if (this.sp != null) {
                /*this.user.setSesskey(this.sp.getString("sessionKey", null));
                this.user.setUserId(this.sp.getString("uid", null));
                this.user.setUser(this.sp.getString("lastUser", null));
                this.user.setPassword(this.sp.getString("up", null));*/
                this.user.setAccess_token(this.sp.getString("access_token", null));
                this.user.setUser_id(Integer.parseInt(this.sp.getString("user_id", null)));
                this.user.setUser_name(this.sp.getString("user_name", null));
                this.user.setUser_avatar(this.sp.getString("user_avatar", null));
                this.user.setClient_id(this.sp.getString("client_id", null));
            }
        }
        return this.user;
    }

    public String load(String paramString) {
        String str = null;
        if (paramString != null) {
            SharedPreferences localSharedPreferences = this.sp;
            str = null;
            if (localSharedPreferences != null) {
                str = this.sp.getString(paramString, null);
            }
        }
        return str;
    }

    public void logout() {
        this.user = null;
        this.user = new LoginUser();
        if (this.sp != null) {
            SharedPreferences.Editor localEditor = this.sp.edit();
            localEditor.remove("lastUser");
            localEditor.remove("uid");
            localEditor.remove("up");
            localEditor.remove("sessionKey");
            localEditor.commit();
        }
    }

    public void save(String paramString1, String paramString2) {
        if ((paramString1 != null) && (paramString2 != null) && (this.sp != null)) {
            SharedPreferences.Editor localEditor = this.sp.edit();
            localEditor.putString(paramString1, paramString2);
            localEditor.commit();
        }
    }

    //    http://api.ibaozou.com/users/verify_bind.app?client_id=10230158&uid=421B53BA2875A168A88B9097C8C5A775&oauth_client_name=qq_connect
    public String getThreadQQLogin(String uid) {
        return " http://api.ibaozou.com/users/verify_bind.app?" +
                "client_id=10230158" +
                "&uid=" + uid +
                "&oauth_client_name=qq_connect";
    }

    //点赞
    public static String getDingCommUrl(String articleId, String id) {
        return "http://api.ibaozou.com/api/v1/articles/" + articleId +
                "/comments/" + id +
                "/up";
    }


    public static String getCaiCommUrl(String paramInt1, String paramInt2) {
        return "http://api.ibaozou.com/api/v1/articles/" + paramInt1 + "/comments/" + paramInt2 + "/dn";
    }

    public static String getReportCommUrl(String paramInt) {
        return "http://api.ibaozou.com/api/v1/comments/" + paramInt + "/report";
    }
}
