package com.qianfeng.android.baoman.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;

import com.qianfeng.android.baoman.MainActivity;
import com.qianfeng.android.baoman.MarkerFindActivity;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.RecourseFindActivity;
import com.qianfeng.android.baoman.TaskFindActivity;
import com.qianfeng.android.baoman.WebMarkerFindActivity;
import com.qianfeng.android.baoman.DownLoadFindActivity;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 发现主界面
 */
public class FindFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final int FIND_RESOURCE = 2;
    public static final int FIND_MARKER = 0;

    private List<Map<String, Object>> data;

    public FindFragment() {

    }


    private MainActivity context;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity != null) {
            context = (MainActivity) activity;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflate = inflater.inflate(R.layout.fragment_find, container, false);

        context.setActionBarTitle("暴走大厅");
        context.setFunctionVisible(false);


        data = new LinkedList<Map<String, Object>>();

        HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("icon", R.drawable.clean_maker_selector);
        map.put("title", "制作器");
        data.add(map);

        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_webmaker_selector);
        map.put("title", "网页制作器");
        data.add(map);


        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_comic_selector);
        map.put("title", "资源");
        data.add(map);


        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_load_selector);
        map.put("title", "离线阅读");
        data.add(map);


        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_game_selector);
        map.put("title", "游戏");
        data.add(map);


        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_bbs_selector);
        map.put("title", "论坛");
        data.add(map);


        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_snack_selector);
        map.put("title", "暴走小店");
        data.add(map);


        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_task_selector);
        map.put("title", "任务中心");
        data.add(map);


        map = new HashMap<String, Object>();
        map.put("icon", R.drawable.clean_checkin_selector);
        map.put("title", "签到");
        data.add(map);


        final SimpleAdapter adapter = new SimpleAdapter(
                getActivity(),
                data,
                R.layout.item_find,
                new String[]{"icon", "title"},
                new int[]{R.id.find_item_icon, R.id.find_item_title}
        );

        GridView gridView = (GridView) inflate.findViewById(R.id.find_fragment_grid_view);
        gridView.setAdapter(adapter);


        //GridView条目点击事件处理
        gridView.setOnItemClickListener(this);

        return inflate;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = null;

        switch (position){
            case 0:    //跳转制作器
                intent = new Intent(getActivity(), MarkerFindActivity.class);
                break;
            case 1:  //跳转网页制作器
                intent = new Intent(getActivity(), WebMarkerFindActivity.class);
                break;
            case 2:   //跳转资源
                intent = new Intent(getActivity(), RecourseFindActivity.class);
                break;
            case 3:  //跳转离线阅读
                intent = new Intent(getActivity(), DownLoadFindActivity.class);
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:  //跳转任务中心
                intent = new Intent(getActivity(), TaskFindActivity.class);
                break;
            case 8:
                break;
        }

        if (intent != null){
            startActivity(intent);
        }
    }
}
