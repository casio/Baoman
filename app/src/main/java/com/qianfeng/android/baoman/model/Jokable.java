package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by zhangkx on 2015/3/10.
 */
public class Jokable implements Serializable {
    /*
                 "alt_score": 81698,
                "comment_status": "",
                "content": "",
                "created_at": "2015-03-10 15:03:10",
                "face_id": "",
                "for_mobile": true,
                "group_id": 19,
                "id": 17355071,
                "neg": -265,
                "original_id": "",
                "pos": 12218,
                "published_at": "2015-03-10 00:00:00",
                "score": 11938,
                "series_id": null,
                "status": "publish",
                "title": "事实证明!那里短但是舌功好也是OK的!",
                "updated_at": "2015-03-10T23:06:39+08:00",
                "user_id": 7163633,
                "pictures": "http://ww4.sinaimg.cn/large/e4b7328cjw1eq0vddehmoj20cs2dp424.jpg",
                "height": 3085,
                "width": 460,
                "small_pictures": "http://wanzao2.b0.upaiyun.com/system/avatars/default/4.png-s1",
                "series": {},
                "user": {
                    "login": "我不叫神龟",
                    "avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/0/original/missing.png-s1",
                    "id": 7163633
                },
                "user_login": "我不叫神龟",
                "user_avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/0/original/missing.png-s1",
                "picture_type": 4,
                "thumb": "http://wanzao2.b0.upaiyun.com/system/pictures/17355071/original/8446263b0bfdbfa9.png-bzthumb",
                "poll_type": 0,
                "poll": {},
                "reward_count": 0,
                "public_comments_count": 432,
                "watched_count": 218,
                "group": {
                    "name": "暴走漫画"
                },
                "ad_url_ios": "",
                "ad_url_android": "",
                "ad_url_winphone": "",
                "ad_url_web": ""

                "mp4_file_link": "http://hc24.aipai.com/user/596/17601596/4590714/card/24738786/card.mp4",
                "mp4_image_link": "http://kindeditor.b0.upaiyun.com/142606523785774eee6ea3e268ddd7.png",


     */

    private String alt_score;   //"alt_score": 81698,
    private String comment_status;   //"comment_status": "",
    private String content;   //"content": "",
    private String created_at;   //"created_at": "2015-03-10 15:03:10",
    private String face_id;   //"face_id": "",
    //            private String for_mobile;   //"for_mobile": true,
    private String group_id;   //"group_id": 19,
    private String id;   //"id": 17355071,
    private String neg;   //"neg": -265,
    private String original_id;   //"original_id": "",
    private String pos;   //"pos": 12218,
    private String published_at;   //"published_at": "2015-03-10 00:00:00",
    private String score;   //"score": 11938,
    private String series_id;   //"series_id": null,
    private String status;   //"status": "publish",
    private String title;   //"title": "事实证明!那里短但是舌功好也是OK的!",
    private String updated_at;   //"updated_at": "2015-03-10T23:06:39+08:00",
    private String user_id;   //"user_id": 7163633,
    private String pictures;   //"pictures": "http://ww4.sinaimg.cn/large/e4b7328cjw1eq0vddehmoj20cs2dp424.jpg",
    private String height;   //"height": 3085,
    private String width;   //"width": 460,
    private String small_pictures;   //"small_pictures": "http://wanzao2.b0.upaiyun.com/system/avatars/default/4.png-s1",
    private String series;   //"series": {},
    private String user_login;   //"user_login": "我不叫神龟",
    private String user_avatar;   //"user_avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/0/original/missing.png-s1",
    private String picture_type;   //"picture_type": 4,
    private String thumb;   //"thumb": "http://wanzao2.b0.upaiyun.com/system/pictures/17355071/original/8446263b0bfdbfa9.png-bzthumb",
    private String poll_type;   //"poll_type": 0,
    private String poll;   //"poll": {},
    private String reward_count;   //"reward_count": 0,
    private String public_comments_count;   //"public_comments_count": 432,
    private String watched_count;   //"watched_count": 218,
    private String ad_url_ios;   //"ad_url_ios": "",
    private String ad_url_android;   //"ad_url_android": "",
    private String ad_url_winphone;   //"ad_url_winphone": "",
    private String ad_url_web;   //"ad_url_web": ""
    private User user;
    private Group group;
    private String mp4_file_link;   //"ad_url_winphone": "",
    private String mp4_image_link;   //"ad_url_web": ""


    public void parseJSON(JSONObject json) {
        if (json != null) {
            try {
                alt_score = json.getString("alt_score"); // 必须存在的字段优先解析，这样保证数据的有效性
                created_at = json.getString("created_at");
                group_id = json.getString("group_id");
                id = json.getString("id");
                published_at = json.getString("published_at");
                score = json.getString("score");
                status = json.getString("status");
                title = json.getString("title");
                updated_at = json.getString("updated_at");
                user_id = json.getString("user_id");


                small_pictures = json.getString("small_pictures");
                user_login = json.getString("user_login");
                user_avatar = json.getString("user_avatar");
                public_comments_count = json.getString("public_comments_count");
                watched_count = json.getString("watched_count");


                pictures = json.optString("pictures");
                thumb = json.optString("thumb");
                height = json.optString("height");
                width = json.optString("width");
                mp4_file_link = json.optString("mp4_file_link");
                mp4_image_link = json.optString("mp4_image_link");


                content = json.optString("content");
                face_id = json.optString("face_id");
                neg = json.optString("neg");
                original_id = json.optString("original_id");
                pos = json.optString("pos");
                series_id = json.optString("series_id");
                series = json.optString("series");
                picture_type = json.optString("picture_type");
                poll_type = json.optString("poll_type");
                poll = json.optString("poll");
                reward_count = json.optString("reward_count");
                comment_status = json.optString("comment_status");
                ad_url_ios = json.optString("ad_url_ios");
                ad_url_android = json.optString("ad_url_android");
                ad_url_winphone = json.optString("ad_url_winphone");
                ad_url_web = json.optString("ad_url_web");


                JSONObject userJson = json.getJSONObject("user");
                user = new User();
                user.parseJSON(userJson);

                JSONObject groupJson = json.getJSONObject("group");
                group = new Group();
                group.parseJSON(groupJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getAlt_score() {
        return alt_score;
    }

    public String getComment_status() {
        return comment_status;
    }

    public String getContent() {
        return content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getFace_id() {
        return face_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public String getId() {
        return id;
    }

    public String getNeg() {
        return neg;
    }

    public String getOriginal_id() {
        return original_id;
    }

    public String getPos() {
        return pos;
    }

    public String getPublished_at() {
        return published_at;
    }

    public String getScore() {
        return score;
    }

    public String getSeries_id() {
        return series_id;
    }

    public String getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getPictures() {
        return pictures;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getSmall_pictures() {
        return small_pictures;
    }

    public String getSeries() {
        return series;
    }

    public String getUser_login() {
        return user_login;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public String getPicture_type() {
        return picture_type;
    }

    public String getThumb() {
        return thumb;
    }

    public String getPoll_type() {
        return poll_type;
    }

    public String getPoll() {
        return poll;
    }

    public String getReward_count() {
        return reward_count;
    }

    public String getPublic_comments_count() {
        return public_comments_count;
    }

    public String getWatched_count() {
        return watched_count;
    }

    public String getAd_url_ios() {
        return ad_url_ios;
    }

    public String getAd_url_android() {
        return ad_url_android;
    }

    public String getAd_url_winphone() {
        return ad_url_winphone;
    }

    public String getAd_url_web() {
        return ad_url_web;
    }

    public User getUser() {
        return user;
    }

    public Group getGroup() {
        return group;
    }

    public String getMp4_file_link() {
        return mp4_file_link;
    }

    public String getMp4_image_link() {
        return mp4_image_link;
    }
}
