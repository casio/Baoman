package com.qianfeng.android.baoman.task;

import android.os.AsyncTask;
import android.util.Log;

import com.qianfeng.android.baoman.util.HttpTool;

import java.util.HashMap;

/**
 * Created by Android_Studio on 2015/3/16.
 * User:Maxiaoyu
 * Date:2015/3/16
 * Email:731436452@qq.com
 */
public class ThirdRegisterTask extends AsyncTask<String, Integer, byte[]> {
    @Override
    protected byte[] doInBackground(String... params) {
        byte[] ret = null;

        HashMap localHashMap = new HashMap();
//        Log.i("test", "提交第三方授权绑定已有用户uid:   " + paramString1);
        String paramString1 = params[0];
        String paramString2 = params[1];
        String paramString3 = params[2];
        String paramString4 = params[3];
        if (params != null && params.length == 4) {
            localHashMap.put("uid", paramString1);
            localHashMap.put("oauth_client_name", paramString2);
            localHashMap.put("user[login]", paramString3);
            localHashMap.put("user[password]", paramString4);
            Log.i("test", "提交第三方授权绑定已有用户url:   " + "http://api.ibaozou.com/users.app?client_id=10230158");
//        String str = bu.doPost("http://api.ibaozou.com/users.app?client_id=10230158", localHashMap);
            ret = HttpTool.post("http://api.ibaozou.com/users.app?client_id=10230158", localHashMap);
            Log.i("test", "提交第三方授权绑定已有用户:   " + new String(ret));
        }
        return ret;
    }
}
