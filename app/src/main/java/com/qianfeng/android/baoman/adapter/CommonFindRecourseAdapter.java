package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.RecourseList;

import java.util.List;


/**
 * Created by ldy on 2015/3/11.
 */

//发现-资源-热门连载/最新上架/最近更新 共用adapter

public class CommonFindRecourseAdapter extends BaseAdapter{

    private List<RecourseList> recourseListList;
    private LayoutInflater inflater;



    public CommonFindRecourseAdapter(Context context , List<RecourseList> recourseListList) {
        if (context != null) {
            inflater = LayoutInflater.from(context);
        }else {
            throw new IllegalArgumentException("Context must be not null");
        }

        this.recourseListList = recourseListList;
    }



    @Override
    public int getCount() {
        int ret = 0;
        if (recourseListList != null) {
            ret = recourseListList.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int position) {
        Object ret = null;
        if (recourseListList != null) {
            ret = recourseListList.get(position);
        }
        return ret;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;

        if (convertView != null) {
            ret = convertView;
        }else {
            ret = inflater.inflate(R.layout.item_common_recourse_find,parent,false);
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.cartoonImg = (ImageView) ret.findViewById(R.id.find_recourse_item_img);
            holder.state = (TextView) ret.findViewById(R.id.find_recourse_item_state);
            holder.title = (TextView) ret.findViewById(R.id.find_recourse_item_title);

            ret.setTag(holder);
        }

        RecourseList recourseList = recourseListList.get(position);

        //显示图片
        holder.cartoonImg.setImageResource(R.drawable.maker_default_face_1);
        String pic = recourseList.getPic();
        holder.cartoonImg.setTag(pic);

        BitmapHelp.getUtils().display(holder.cartoonImg,recourseList.getPic(),new BitmapLoadCallBack<ImageView>() {
            @Override
            public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

            }
        });



        //显示标题
        String name = recourseList.getName();
        holder.title.setText(name);

        //显示状态
        String list_type = recourseList.getList_type();
        holder.state.setText(list_type);


        return ret;
    }

    private static class ViewHolder{
        private ImageView cartoonImg;  //漫画的图片
        private TextView state;   //漫画的状态
        private TextView title;   //漫画标题

    }
}
