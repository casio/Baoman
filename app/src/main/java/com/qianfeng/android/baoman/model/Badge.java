package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Android_Studio on 2015/3/14.
 * User:Maxiaoyu
 * Date:2015/3/14
 * Email:731436452@qq.com
 */
public class Badge implements Serializable {
    private int id;// 15,
    private int badge_type_id;// 1,
    private String title;// "薄丝狂顶",
    private String description;// "BOSS级人物诞生！突破30000顶UP！！恭喜你！！！你已经被评为老大，拿着这个勋章可以随意招摇......但是你要记得，我王尼玛在等着你~",
    private String icon;// "http://wanzao2.b0.upaiyun.com/system/icons/15/original/D9_%E5%89%AF%E6%9C%AC.png",
    private String small_icon;// "http://wanzao2.b0.upaiyun.com/system/icons/15/original/D9_%E5%89%AF%E6%9C%AC.png-s1"


    @Override
    public boolean equals(Object o) {
        if (o instanceof Badge) {
            Badge badge = (Badge) o;
            return this.id == badge.getId();
        }
        return super.equals(o);
    }

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                id = jsonObject.getInt("id");// 15,
                badge_type_id = jsonObject.getInt("badge_type_id");// 1,
                title = jsonObject.getString("title");// "薄丝狂顶",
                description = jsonObject.getString("description");// "BOSS级人物诞生！突破30000顶UP！！恭喜你！！！你已经被评为老大，拿着这个勋章可以随意招摇......但是你要记得，我王尼玛在等着你~",
                icon = jsonObject.getString("icon");// "http://wanzao2.b0.upaiyun.com/system/icons/15/original/D9_%E5%89%AF%E6%9C%AC.png",
                small_icon = jsonObject.getString("small-icon");// "http://wanzao2.b0.upaiyun.com/system/icons/15/original/D9_%E5%89%AF%E6%9C%AC.png-s1"
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBadge_type_id() {
        return badge_type_id;
    }

    public void setBadge_type_id(int badge_type_id) {
        this.badge_type_id = badge_type_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSmall_icon() {
        return small_icon;
    }

    public void setSmall_icon(String small_icon) {
        this.small_icon = small_icon;
    }
}
