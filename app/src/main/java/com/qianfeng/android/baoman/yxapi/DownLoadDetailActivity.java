package com.qianfeng.android.baoman.yxapi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.adapter.CommonImagePagerAdapter;

import java.io.File;
import java.util.LinkedList;

public class DownLoadDetailActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_down_load_detail);
        ViewPager pager = (ViewPager) findViewById(R.id.download_load_detail_pager);
        Intent intent = getIntent();
        if (intent != null) {
            File dirs = (File) intent.getSerializableExtra("dirs");
            LinkedList<Bitmap> bitmaps = new LinkedList<>();
            if (dirs != null) {
                File[] files = dirs.listFiles();
//                for (File file : files) {
//                    String path = file.getAbsolutePath();
//                    Bitmap bitmap = BitmapFactory.decodeFile(path);
//                    bitmaps.add(bitmap);
//                }
                for (int i = 0; i < 5; i++) {
                    String path = files[i].getAbsolutePath();
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    bitmaps.add(bitmap);
                }
                CommonImagePagerAdapter commonImagePagerAdapter = new CommonImagePagerAdapter(bitmaps, this);
                pager.setAdapter(commonImagePagerAdapter);
            }
        }
    }
}
