package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Android_Studio on 2015/3/11.
 * User:Maxiaoyu
 * Date:2015/3/11
 * Email:731436452@qq.com
 */
public class Series implements Serializable {
    private int id;// 1753,
    private String name;// "编辑部的故事",
    private String icon;// "http://wanzao2.b0.upaiyun.com/system/series/icons/1753/original/1753.jpg",
    private String description;// "王尼玛和他的编辑部成员们成天在做些什么呢？他们当中到底谁是基佬，谁是蛇精病，谁是正常狗？这些问题在暴走编辑部的故事中，我们也不会告诉你的。",
    private String user_name;// "稻草小哪吒",
    private int user_id;//177237

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                id = jsonObject.getInt("id");
                name = jsonObject.getString("name");
                icon = jsonObject.getString("icon");
                description = jsonObject.getString("description");
                user_name = jsonObject.getString("user_name");
                user_id = jsonObject.getInt("user_id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
