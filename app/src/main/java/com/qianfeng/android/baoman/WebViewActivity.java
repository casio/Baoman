package com.qianfeng.android.baoman;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.qianfeng.android.baoman.model.WebMaker;

import java.util.ArrayList;
import java.util.LinkedList;


public class WebViewActivity extends Activity implements View.OnClickListener {

    private String url;
    private String name;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_web_view);

        Intent intent = getIntent();
        if (intent != null) {
            url = intent.getStringExtra("url");
            name = intent.getStringExtra("name");
        }


        TextView titleTextView = (TextView) findViewById(R.id.find_web_top_title);
        titleTextView.setText(name);

        ImageView backImg = (ImageView) findViewById(R.id.find_web_top_back);
        backImg.setOnClickListener(this);

        // 用于显示网页
        WebView webView = (WebView) findViewById(R.id.webView);

        // WebView 如果希望在程序内部显示网页，而不是打开浏览器，那么WebView 必须设置 WebViewClient 变量
        // 默认可以设置一个 WebViewClient 这样就不会打开浏览器了，
        // webviewclient 用于实现自定义网址功能的累
        webView.setWebViewClient(new WebViewClient());


        // loadUrl 代表让WebView 显示网页，支持 http:// https:// javascript:// file:/// 等协议
        //      以及 支持 file:///android_asset/ 访问  assets 目录中的网页
        webView.loadUrl(url);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.find_web_top_back:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = LayoutInflater.from(this);
                builder.setTitle("即将退出页面")
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                                dialogInterface.dismiss();
                            }
                        });

                AlertDialog dialog = builder.create();

                dialog.show();

                break;
        }
    }
}
