package com.qianfeng.android.baoman;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qianfeng.android.baoman.adapter.CommonFindRecourseAdapter;
import com.qianfeng.android.baoman.adapter.MoreCommonRecourseAdapter;
import com.qianfeng.android.baoman.model.CartoonLatestInfo;
import com.qianfeng.android.baoman.model.RecourseList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


public class CommonRecourseFindActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener ,Serializable{

    private String url;
    private MoreCommonRecourseAdapter adapter;
    private PullToRefreshListView pullToRefreshListView;
    private List<RecourseList> recourseLists;
    private LinearLayout load_layout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_recourse_find);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);
        String hotUrl = intent.getStringExtra("hotUrl");
        String addUrl = intent.getStringExtra("addUrl");
        String updateUrl = intent.getStringExtra("updateUrl");
        String classicsEndUrl = intent.getStringExtra("classicsEndUrl");
        String shortFourUrl = intent.getStringExtra("shortFourUrl");
        String wonderfulChoiceUrl = intent.getStringExtra("wonderfulChoiceUrl");
        String weekUrl = intent.getStringExtra("weekUrl");
        String monthUrl = intent.getStringExtra("monthUrl");
        String yearUrl = intent.getStringExtra("yearUrl");


        TextView title = (TextView) findViewById(R.id.find_common_recourse_top_title);

        ImageView backImg = (ImageView) findViewById(R.id.find_common_recourse_top_back);
        backImg.setOnClickListener(this);


        if (R.id.find_recourse_hot_serialize_more_txt == id ||
                R.id.find_recourse_hot_serialize_more == id) {
            title.setText("热门连载");
            url = hotUrl;
        }

        if (R.id.find_recourse_last_add_more_txt == id ||
                R.id.find_recourse_last_add_more == id) {
            title.setText("最新上架");
            url = addUrl;
        }

        if (R.id.find_recourse_last_update_more_txt == id ||
                R.id.find_recourse_last_update_more == id) {
            title.setText("最近更新");
            url = updateUrl;
        }

        if (id == R.id.find_recourse_classics_end_txt) {
            url = classicsEndUrl;
            title.setText("经典完结");
        }
        if (id == R.id.find_recourse_short_four_txt) {
            url = shortFourUrl;
            title.setText("短片四格");
        }
        if (id == R.id.find_recourse_wonderful_choice_txt) {
            url = wonderfulChoiceUrl;
            title.setText("全彩精彩");
        }
        if (id == R.id.find_recourse_week_ranked_txt) {
            url = weekUrl;
            title.setText("周排行");
        }
        if (id == R.id.find_recourse_month_ranked_txt) {
            url = monthUrl;
            title.setText("月排行");
        }
        if (id == R.id.find_recourse_year_ranked_txt) {
            url = yearUrl;
            title.setText("年排行");
        }


        load_layout = (LinearLayout) findViewById(R.id.load_layout);

        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.common_recourse_list_view);
        pullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
        ListView listView = pullToRefreshListView.getRefreshableView();

        recourseLists = new LinkedList<RecourseList>();

        adapter = new MoreCommonRecourseAdapter(this, recourseLists);

        if (listView != null) {
            listView.setAdapter(adapter);
            getHttpUtils(url);
            listView.setOnItemClickListener(this);
        }


    }

    private void getHttpUtils(String url) {
        if (recourseLists == null) {
            load_layout.setVisibility(View.VISIBLE);
        }

        HttpUtils httpUtils = new HttpUtils();
        httpUtils.send(
                HttpRequest.HttpMethod.GET,
                url,
                new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> jsonObjectResponseInfo) {
                        String result = jsonObjectResponseInfo.result;

                        LinkedList<RecourseList> recourse = new LinkedList<RecourseList>();

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray listsArray = jsonObject.getJSONArray("lists");

                            for (int i = 0; i < listsArray.length(); i++) {


                                JSONObject recourseArrayJSONObject = listsArray.getJSONObject(i);

                                JSONObject latest_info = recourseArrayJSONObject.getJSONObject("latest_info");

                                CartoonLatestInfo cartoonLatestInfo = new CartoonLatestInfo();
                                cartoonLatestInfo.parseJSON(latest_info);

                                RecourseList recourseList = new RecourseList();

                                recourseList.setLatestInfo(cartoonLatestInfo);
                                recourseList.parseJSON(recourseArrayJSONObject);

                                recourseList.setLatestInfo(cartoonLatestInfo);
                                recourse.add(recourseList);
                            }

                            recourseLists.addAll(recourse);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        load_layout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(CommonRecourseFindActivity.this, "连接失败,网络问题", Toast.LENGTH_SHORT).show();
                        load_layout.setVisibility(View.GONE);
                    }
                }
        );
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.find_common_recourse_top_back) {
            finish();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        Intent intent = null;

        intent = new Intent(this, CartoonItemDetailsActivity.class);
        RecourseList recourseList = recourseLists.get(position);
        intent.putExtra("recourseList",recourseList);

        startActivity(intent);
    }
}
