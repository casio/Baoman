package com.qianfeng.android.baoman;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qianfeng.android.baoman.adapter.CartoonItemDetailsAdapter;
import com.qianfeng.android.baoman.model.CartoonInfos;
import com.qianfeng.android.baoman.model.RecourseList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


public class CartoonItemDetailsActivity extends Activity implements View.OnClickListener {


    private CartoonItemDetailsAdapter adapter;
    private List<CartoonInfos> cartoonInfos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_cartoon_item_details);

        //图片
        ImageView img = (ImageView) findViewById(R.id.find_cartoon_details_img);
        //标题
        TextView title = (TextView) findViewById(R.id.find_cartoon_details_title);
        //描述
        TextView description = (TextView) findViewById(R.id.find_cartoon_details_description);
        //作者
        TextView author = (TextView) findViewById(R.id.find_cartoon_details_author);
        //状态：连载、完结
        TextView state = (TextView) findViewById(R.id.find_cartoon_details_state);
        //返回键
        ImageView backBtn = (ImageView) findViewById(R.id.find_cartoon_details_top_back);
        backBtn.setOnClickListener(this);

        cartoonInfos = new LinkedList<CartoonInfos>();
        GridView cartoonNumGridView = (GridView) findViewById(R.id.cartoon_details_grid_view);
        adapter = new CartoonItemDetailsAdapter(this, cartoonInfos);


        Intent intent = getIntent();

        //热门连载
        RecourseList hotRecourse = (RecourseList) intent.getSerializableExtra("hotRecourse");

        String url;
        int id;
        if (hotRecourse != null) {

            int hotId = Integer.parseInt(hotRecourse.getId());

            BitmapHelp.getUtils().display(img,hotRecourse.getPic(),new BitmapLoadCallBack<ImageView>() {
                @Override
                public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                    imageView.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

                }
            });

            title.setText(hotRecourse.getName());
            description.setText(hotRecourse.getDescription());
            author.setText("作者："+hotRecourse.getAuthor_name());
            state.setText(hotRecourse.getList_type());
            id = hotId;
            url = "http://hahaapi.ibaozou.com/api/v1/lists/" + id + ".json?per_page=999999";
            getHttpUtils(url, adapter);

        }


        //最新上架
        RecourseList addRecourse = (RecourseList) intent.getSerializableExtra("addRecourse");

        if (addRecourse != null) {
            int addId = Integer.parseInt(addRecourse.getId());

            BitmapHelp.getUtils().display(img, addRecourse.getPic(), new BitmapLoadCallBack<ImageView>() {
                @Override
                public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                    imageView.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

                }
            });

            title.setText(addRecourse.getName());
            description.setText(addRecourse.getDescription());
            author.setText("作者："+addRecourse.getAuthor_name());
            state.setText(addRecourse.getList_type());
            id = addId;
            url = "http://hahaapi.ibaozou.com/api/v1/lists/" + id + ".json?per_page=999999";
            getHttpUtils(url, adapter);

        }


        //最近更新
        RecourseList updateRecourse = (RecourseList) intent.getSerializableExtra("updateRecourse");

        if (updateRecourse != null) {
            int updateId = Integer.parseInt(updateRecourse.getId());

            BitmapHelp.getUtils().display(img,updateRecourse.getPic(),new BitmapLoadCallBack<ImageView>() {
                @Override
                public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                    imageView.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

                }
            });

            title.setText(updateRecourse.getName());
            description.setText(updateRecourse.getDescription());
            author.setText("作者："+updateRecourse.getAuthor_name());
            state.setText(updateRecourse.getList_type());
            id = updateId;
            url = "http://hahaapi.ibaozou.com/api/v1/lists/" + id + ".json?per_page=999999";
            getHttpUtils(url, adapter);

        }

        //月、周、年排行  经典完结。。。。。
        RecourseList recourseList = (RecourseList) intent.getSerializableExtra("recourseList");
        if (recourseList != null) {
            int commonId = Integer.parseInt(recourseList.getId());

            BitmapHelp.getUtils().display(img,recourseList.getPic(),new BitmapLoadCallBack<ImageView>() {
                @Override
                public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                    imageView.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

                }
            });

            title.setText(recourseList.getName());
            description.setText(recourseList.getDescription());
            author.setText("作者："+recourseList.getAuthor_name());
            state.setText(recourseList.getList_type());
            id = commonId;
            url = "http://hahaapi.ibaozou.com/api/v1/lists/" + id + ".json?per_page=999999";
            getHttpUtils(url, adapter);

        }


        cartoonNumGridView.setAdapter(adapter);



    }


    private void getHttpUtils(String url, final CartoonItemDetailsAdapter adapter) {

        HttpUtils httpUtils = new HttpUtils();
        httpUtils.send(HttpRequest.HttpMethod.GET, url, new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> jsonObjectResponseInfo) {
                        String result = jsonObjectResponseInfo.result;

                        LinkedList<CartoonInfos> infoList = new LinkedList<CartoonInfos>();

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray listsArray = jsonObject.getJSONArray("infos");

                            for (int i = 0; i < listsArray.length(); i++) {


                                JSONObject recourseArrayJSONObject = listsArray.getJSONObject(i);

                                CartoonInfos infos = new CartoonInfos();

                                infos.parseJson(recourseArrayJSONObject);

                                infoList.add(infos);

                            }
                            cartoonInfos.addAll(infoList);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(CartoonItemDetailsActivity.this, "连接失败,网络问题", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.find_cartoon_details_top_back:
                finish();
                break;
        }
    }
}
