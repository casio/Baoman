package com.qianfeng.android.baoman.task;

import org.json.JSONObject;

/**
 * Created by Android_Studio on 2015/3/16.
 * User:Maxiaoyu
 * Date:2015/3/16
 * Email:731436452@qq.com
 */
public interface TaskInterface {
    public void backResult(JSONObject jsonObject);
}
