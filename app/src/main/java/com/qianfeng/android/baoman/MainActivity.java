package com.qianfeng.android.baoman;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.VideoView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.qianfeng.android.baoman.fragment.FindFragment;
import com.qianfeng.android.baoman.fragment.MovieFragment;
import com.qianfeng.android.baoman.fragment.PagerFragment;
import com.qianfeng.android.baoman.fragment.TopicFragment;
import com.qianfeng.android.baoman.fragment.UserFragment;
import com.qianfeng.android.baoman.model.User;

import java.util.LinkedList;
import java.util.List;


public class MainActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

//    private List<Fragment> fragments;
    private SlidingMenu slidingMenu;
    private ImageButton btnGanhuo;
    private ImageButton btnGif;
    private ImageButton btnNaocan;
    private ImageButton btnQutu;
    private ImageButton btnTucao;
    private ImageButton btnNencao;
    private ImageButton btnBaozou;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //slidingmenu
        slidingMenu = new SlidingMenu(this, SlidingMenu.SLIDING_CONTENT);

        slidingMenu.setMenu(R.layout.slidingmenu);

        //获取屏幕宽度的3/7
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay();
        int width = d.getWidth();
        int slidingmenuWidth = width / 11 * 6;
        slidingMenu.setBehindOffset(slidingmenuWidth);

        //边界滑动，可以避免与viewpager的冲突
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        //属性动画

        ImageView ico = (ImageView) findViewById(R.id.main_splash_ico);
        ObjectAnimator.ofFloat(ico, "translationX", ico.getX(), width).setDuration(2000).start();
        ObjectAnimator.ofFloat(ico, "rotationX", 0.0F, 360.0F).setDuration(1000).start();
        //初始化BitmapHelp!
        BitmapHelp.initUtils(this);
//        fragments = new LinkedList<>();
////      添加所有的Fragment
//        TopicFragment topicFragment = new TopicFragment();
//        fragments.add(topicFragment);
//        fragments.add(new MovieFragment());
//        fragments.add(new FindFragment());
//        fragments.add(new PagerFragment());
//        fragments.add(new UserFragment());

        //设置默认的RadioButton点击事件
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.bottomActionbar);
        if (radioGroup != null) {
            radioGroup.setOnCheckedChangeListener(this);
            View view = radioGroup.getChildAt(0);
            if (view != null && view instanceof RadioButton) {
                RadioButton rb = (RadioButton) view;
//                设置RadioButton被点击
                rb.setChecked(true);
            }
        }
        //////////////////////////SlidingMenu相关设置/////////////////////////
        btnBaozou = (ImageButton) findViewById(R.id.sliding_baozou);

        btnGanhuo = (ImageButton) findViewById(R.id.sliding_ganhuo);
        btnGanhuo.setBackgroundResource(R.drawable.ganhuo_pressed);
        btnGif = (ImageButton) findViewById(R.id.sliding_gif);
        btnNaocan = (ImageButton) findViewById(R.id.sliding_naocan);
        btnQutu = (ImageButton) findViewById(R.id.sliding_qutu);
        btnTucao = (ImageButton) findViewById(R.id.sliding_tucao);
        btnNencao = (ImageButton) findViewById(R.id.sliding_nencao);

        btnBaozou.setOnClickListener(this);
        btnGanhuo.setOnClickListener(this);
        btnGif.setOnClickListener(this);
        btnNaocan.setOnClickListener(this);
        btnQutu.setOnClickListener(this);
        btnTucao.setOnClickListener(this);
        btnNencao.setOnClickListener(this);
    }

    /**
     * RadioButton点击事件
     *
     * @param group
     * @param checkedId
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int count = group.getChildCount();
        int position = -1;
        for (int i = 0; i < count; i++) {
            View view = group.getChildAt(i);
            if (view instanceof RadioButton) {
                RadioButton rb = (RadioButton) view;
                if (rb.isChecked()) {
                    position = i;
                    break;
                }
            }
        }
        Intent intent=null;
        switch (position){
            case 0:
                intent=new Intent(this,TopicActivity.class);
                break;
            case 1:
                intent=new Intent(this,MovieActivity.class);
                break;
            case 2:
                intent=new Intent(this,FindActivity.class);
                break;
            case 3:
                intent=new Intent(this,PagerActivity.class);
                break;
            case 4:
                intent=new Intent(this,UserActivity.class);
                break;

        }
        if (intent != null) {
            startActivity(intent);
        }
    }
    /*
      干货 1
      脑残 295
      GIF 25
      暴走 19
      趣图 24
      神吐槽 28
      嫩草 1/latest
       */
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//        switch (id) {
//            case R.id.sliding_baozou:
//
//
//                btnBaozou.setBackgroundResource(R.drawable.ganhuo_pressed);
//                btnGanhuo.setBackgroundResource(R.drawable.ganhuo);
//                btnGif.setBackgroundResource(R.drawable.gif);
//                btnNaocan.setBackgroundResource(R.drawable.naocan);
//                btnNencao.setBackgroundResource(R.drawable.nencao);
//                btnQutu.setBackgroundResource(R.drawable.qutu);
//                btnTucao.setBackgroundResource(R.drawable.tucao);
//
//
//                //请求网络
//                setActionBarTitle("暴走漫画");
//                kindId = 19;
//                flag = true;
//                String url = String.format(URL1, String.valueOf(kindId), kind, "1", String.valueOf(size));
//                executeHttp(url, URL2);
//                slidingMenu.toggle();
//                break;
//            case R.id.sliding_ganhuo:
//                btnBaozou.setBackgroundResource(R.drawable.baozou);
//                btnGanhuo.setBackgroundResource(R.drawable.ganhuo_pressed);
//                btnGif.setBackgroundResource(R.drawable.gif);
//                btnNaocan.setBackgroundResource(R.drawable.naocan);
//                btnNencao.setBackgroundResource(R.drawable.nencao);
//                btnQutu.setBackgroundResource(R.drawable.qutu);
//                btnTucao.setBackgroundResource(R.drawable.tucao);
//
//
//                //请求网络
//                mainActivity.setActionBarTitle("最热门");
//                kindId = 1;
//                flag = true;
//                url = String.format(URL1, String.valueOf(kindId), kind, "1", String.valueOf(size));
//                executeHttp(url, URL2);
//                slidingMenu.toggle();
//                break;
//            case R.id.sliding_gif:
//                btnBaozou.setBackgroundResource(R.drawable.ganhuo);
//                btnGanhuo.setBackgroundResource(R.drawable.ganhuo);
//                btnGif.setBackgroundResource(R.drawable.gif_pressed);
//                btnNaocan.setBackgroundResource(R.drawable.naocan);
//                btnNencao.setBackgroundResource(R.drawable.nencao);
//                btnQutu.setBackgroundResource(R.drawable.qutu);
//                btnTucao.setBackgroundResource(R.drawable.tucao);
//
//
//                //请求网络
//                mainActivity.setActionBarTitle("GIF怪兽");
//                kindId = 25;
//                flag = true;
//                kind = "recent_hot";
//                url = String.format(URL1, String.valueOf(kindId), kind, "1", String.valueOf(size));
//                executeHttp(url, URL2);
//                slidingMenu.toggle();
//
//                break;
//            case R.id.sliding_naocan:
//                btnBaozou.setBackgroundResource(R.drawable.ganhuo);
//                btnGanhuo.setBackgroundResource(R.drawable.ganhuo);
//                btnGif.setBackgroundResource(R.drawable.gif);
//                btnNaocan.setBackgroundResource(R.drawable.naocan_pressed);
//                btnNencao.setBackgroundResource(R.drawable.nencao);
//                btnQutu.setBackgroundResource(R.drawable.qutu);
//                btnTucao.setBackgroundResource(R.drawable.tucao);
//
//
//                //请求网络
//                mainActivity.setActionBarTitle("脑残对话");
//                kindId = 295;
//                flag = true;
//                kind = "recent_hot";
//                url = String.format(URL1, String.valueOf(kindId), kind, "1", String.valueOf(size));
//                executeHttp(url, URL2);
//                slidingMenu.toggle();
//                break;
//            case R.id.sliding_nencao:
//                btnBaozou.setBackgroundResource(R.drawable.ganhuo);
//                btnGanhuo.setBackgroundResource(R.drawable.ganhuo);
//                btnGif.setBackgroundResource(R.drawable.gif);
//                btnNaocan.setBackgroundResource(R.drawable.naocan);
//                btnNencao.setBackgroundResource(R.drawable.nencao_pressed);
//                btnQutu.setBackgroundResource(R.drawable.qutu);
//                btnTucao.setBackgroundResource(R.drawable.tucao);
//
//                //请求网络
//                mainActivity.setActionBarTitle("最新更新");
//                kindId = 1;
//                kind = "latest";
//                flag = true;
//                url = String.format(URL1, String.valueOf(kindId), kind, "1", String.valueOf(size));
//                executeHttp(url, URL2);
//                slidingMenu.toggle();
//                break;
//            case R.id.sliding_qutu:
//                btnBaozou.setBackgroundResource(R.drawable.ganhuo);
//                btnGanhuo.setBackgroundResource(R.drawable.ganhuo);
//                btnGif.setBackgroundResource(R.drawable.gif);
//                btnNaocan.setBackgroundResource(R.drawable.naocan);
//                btnNencao.setBackgroundResource(R.drawable.nencao);
//                btnQutu.setBackgroundResource(R.drawable.qutu_pressed);
//                btnTucao.setBackgroundResource(R.drawable.tucao);
//
//                //请求网络
//                mainActivity.setActionBarTitle("趣图百科");
//                kindId = 24;
//                flag = true;
//                kind = "recent_hot";
//                url = String.format(URL1, String.valueOf(kindId), kind, "1", String.valueOf(size));
//                executeHttp(url, URL2);
//                slidingMenu.toggle();
//                break;
//            case R.id.sliding_tucao:
//                btnBaozou.setBackgroundResource(R.drawable.ganhuo);
//                btnGanhuo.setBackgroundResource(R.drawable.ganhuo);
//                btnGif.setBackgroundResource(R.drawable.gif);
//                btnNaocan.setBackgroundResource(R.drawable.naocan);
//                btnNencao.setBackgroundResource(R.drawable.nencao);
//                btnQutu.setBackgroundResource(R.drawable.qutu_pressed);
//                btnTucao.setBackgroundResource(R.drawable.tucao);
//                //请求网络
//                mainActivity.setActionBarTitle("神吐槽");
//                kindId = 28;
//                flag = true;
//                kind = "recent_hot";
//                url = String.format(URL1, String.valueOf(kindId), kind, "1", String.valueOf(size));
//                executeHttp(url, URL2);
//                slidingMenu.toggle();
//                break;
//        }
//    }
}
