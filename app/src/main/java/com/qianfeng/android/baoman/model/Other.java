package com.qianfeng.android.baoman.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Android_Studio on 2015/3/14.
 * User:Maxiaoyu
 * Date:2015/3/14
 * Email:731436452@qq.com
 */
public class Other implements Serializable {
    private int id;// 3970212,
    private String login;// "凌澈爱上纯",
    private String created_at;// "2014-03-01 17:17:42",
    private String avatar;// "http://wanzao2.b0.upaiyun.com/system/avatars/3970212/original/20150309185530592.png-s1",
    private int public_articles_count;// 21,
    private int articles_count;// 21,
    private int salary;// 2738,
    private int friends_count;// 9,
    private int followers_count;// 180,
    private String email;// "1758546738@qq.com",
    private boolean active;// true,
    private int favorites_count;// 23,
    private boolean following;// false,
    private int pos_count;// 9746,
    private int view_count;// 378,
    private int new_followers;// 0,
    private int new_messages;// 0,

    private List<Badge> badgeList;

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Badge> getBadgeList() {
        return badgeList;
    }

    public void setBadgeList(List<Badge> badgeList) {
        this.badgeList = badgeList;
    }

    public Other() {
        this.badgeList = new LinkedList<>();
    }

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                id = jsonObject.getInt("id");// 3970212,
                login = jsonObject.getString("login");// "凌澈爱上纯",
                created_at = jsonObject.getString("created_at");// "2014-03-01 17:17:42",
                avatar = jsonObject.getString("avatar");// "http://wanzao2.b0.upaiyun.com/system/avatars/3970212/original/20150309185530592.png-s1",
                public_articles_count = jsonObject.getInt("public_articles_count");// 21,
                articles_count = jsonObject.getInt("articles_count");// 21,
                salary = jsonObject.getInt("salary");// 2738,
                friends_count = jsonObject.getInt("friends_count");// 9,
                followers_count = jsonObject.getInt("followers_count");// 180,
                email = jsonObject.getString("email");// "1758546738@qq.com",
                active = jsonObject.getBoolean("active");// "active": true,
                favorites_count = jsonObject.getInt("favorites_count");// 23,
                following = jsonObject.getBoolean("following");// "following": false,
                pos_count = jsonObject.getInt("pos_count");// 9746,
                view_count = jsonObject.getInt("view_count");// 378,
                new_followers = jsonObject.optInt("new_followers");// 0,
                new_messages = jsonObject.optInt("new_messages");// 0,

                JSONArray badges = jsonObject.getJSONArray("badges");
                for (int i = 0; i < badges.length(); i++) {
                    JSONObject object = badges.getJSONObject(i);
                    Badge badge = new Badge();
                    badge.parseJSON(object);
                    badgeList.add(badge);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getPublic_articles_count() {
        return public_articles_count;
    }

    public void setPublic_articles_count(int public_articles_count) {
        this.public_articles_count = public_articles_count;
    }

    public int getArticles_count() {
        return articles_count;
    }

    public void setArticles_count(int articles_count) {
        this.articles_count = articles_count;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(int friends_count) {
        this.friends_count = friends_count;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getFavorites_count() {
        return favorites_count;
    }

    public void setFavorites_count(int favorites_count) {
        this.favorites_count = favorites_count;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public int getPos_count() {
        return pos_count;
    }

    public void setPos_count(int pos_count) {
        this.pos_count = pos_count;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public int getNew_followers() {
        return new_followers;
    }

    public void setNew_followers(int new_followers) {
        this.new_followers = new_followers;
    }

    public int getNew_messages() {
        return new_messages;
    }

    public void setNew_messages(int new_messages) {
        this.new_messages = new_messages;
    }

    @Override
    public String toString() {
        return "Other{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", created_at='" + created_at + '\'' +
                ", avatar='" + avatar + '\'' +
                ", public_articles_count=" + public_articles_count +
                ", articles_count=" + articles_count +
                ", salary=" + salary +
                ", friends_count=" + friends_count +
                ", followers_count=" + followers_count +
                ", email='" + email + '\'' +
                ", active=" + active +
                ", favorites_count=" + favorites_count +
                ", following=" + following +
                ", pos_count=" + pos_count +
                ", view_count=" + view_count +
                ", new_followers=" + new_followers +
                ", new_messages=" + new_messages +
                ", badgeList=" + badgeList +
                '}';
    }
}
