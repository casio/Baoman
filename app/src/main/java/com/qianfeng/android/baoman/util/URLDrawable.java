package com.qianfeng.android.baoman.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.Display;

import com.qianfeng.android.baoman.R;

public class URLDrawable extends BitmapDrawable {
    protected Drawable drawable;

    @SuppressWarnings("deprecation")
    public URLDrawable(Context context, String path) {
        this.setBounds(getDefaultImageBounds(context));
        Bitmap image = ImageCache.getInstance().getImage(path);
        if (image != null) {
            drawable = new BitmapDrawable(image);
        } else {
            drawable = context.getResources().getDrawable(R.drawable.face21);
        }

        drawable.setBounds(getDefaultImageBounds(context));
    }

    @Override
    public void draw(Canvas canvas) {
        if (drawable != null) {
            drawable.draw(canvas);
        }
    }

    @SuppressWarnings("deprecation")
    public Rect getDefaultImageBounds(Context context) {
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        int width = 80;
        int height = 80;

        Rect bounds = new Rect(0, 0, width, height);
        return bounds;
    }
}