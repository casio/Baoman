package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Android_Studio on 2015/3/16.
 * User:Maxiaoyu
 * Date:2015/3/16
 * Email:731436452@qq.com
 */
public class LoginUser implements Serializable {
    private String client_id;// "10230158",
    private String user_name;// "731436452",
    private int user_id;// 8900345,
    private String user_avatar;// "http://wanzao2.b0.upaiyun.com/system/avatars/0/original/missing.png-s1",
    private String access_token;// "f2d25646406519dff5dea1ec510f207ba50dc06b"

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                client_id = jsonObject.getString("client_id");// "10230158",
                user_name = jsonObject.getString("user_name");// "731436452",
                user_id = jsonObject.getInt("user_id");// 8900345,
                user_avatar = jsonObject.getString("user_avatar");// "http://wanzao2.b0.upaiyun.com/system/avatars/0/original/missing.png-s1",
                access_token = jsonObject.getString("access_token");// "f2d25646406519dff5dea1ec510f207ba50dc06b"
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
