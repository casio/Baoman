package com.qianfeng.android.baoman;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.core.BitmapSize;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qianfeng.android.baoman.adapter.CinemaCommentAdapter;
import com.qianfeng.android.baoman.model.CinemaComment;
import com.qianfeng.android.baoman.model.CinemaSubItem;
import com.qianfeng.android.baoman.model.Face;
import com.qianfeng.android.baoman.model.LoginUser;
import com.qianfeng.android.baoman.model.User;
import com.qianfeng.android.baoman.task.CaiTask;
import com.qianfeng.android.baoman.task.DingTask;
import com.qianfeng.android.baoman.task.ReportTask;
import com.qianfeng.android.baoman.task.TaskInterface;
import com.qianfeng.android.baoman.util.LoginUtils;
import com.qianfeng.android.baoman.util.URLImageGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;


public class CinemaCommentActivity extends BaseActivity implements AdapterView.OnItemClickListener, PullToRefreshBase.OnRefreshListener2<ListView>, RadioGroup.OnCheckedChangeListener, Animation.AnimationListener, TaskInterface, AbsListView.OnScrollListener {
    private LinearLayout load_layout;
    private PullToRefreshListView refreshListView;
    private ImageView image_error;
    private int pageCount;

    private List<CinemaComment> cinemaCommentList;
    private int commentId;
    private CinemaCommentAdapter adapter;
    //最热议best  最新 new 最高 high 最好玩desc
    private String type = "desc";
    private ListView listView;
    private Animation animation;
    private ImageView imageDing;
    private ImageView imageCai;
    private ImageView imageFinish;
    private CinemaSubItem cinemaSubItem;
    //代表是否展开  添加或删除   操作（点赞，踩，评，回复）按钮
    private boolean flag = false;
    //代表点击的第条目的Id;
    private int lastId = -1;
    //保存上一次点击条目的位置
    private int position = -1;
    //上一次点击条目对象的层级
    private int lastType = -1;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinema_comment);

        setBackVisible(true);
        imageDing = (ImageView) findViewById(R.id.activity_cinema_comment_ding);
        imageCai = (ImageView) findViewById(R.id.activity_cinema_comment_cai);
        imageFinish = (ImageView) findViewById(R.id.activity_cinema_comment_ding_finish);


        load_layout = (LinearLayout) findViewById(R.id.load_layout);

        image_error = (ImageView) findViewById(R.id.cinemaComment_image_error);
        image_error.setOnClickListener(this);

        //设置动画
        animation = AnimationUtils.loadAnimation(this, R.anim.anim_ding);
        //设置动画监听
        animation.setAnimationListener(this);
        //表情列表
        if (Constants.faceList.size() == 0) {
            loadFaceData(Constants.MOVIE_ALL_FACE);
        }
        //设置标题
        Intent intent = getIntent();
        if (intent != null) {
            cinemaSubItem = (CinemaSubItem) intent.getSerializableExtra("cinemaSubItem");
            if (cinemaSubItem != null) {
                setActionBarTitle(cinemaSubItem.getTitle());
            }

            refreshListView = (PullToRefreshListView) findViewById(R.id.cinemaComment_pullRoRefreshList);
            refreshListView.setMode(PullToRefreshBase.Mode.BOTH);
            listView = refreshListView.getRefreshableView();

            cinemaCommentList = new LinkedList<>();
            adapter = new CinemaCommentAdapter(cinemaCommentList, this);
            adapter.setListener(this);
            View view = LayoutInflater.from(this).inflate(R.layout.item_cinema_comment_1, null, false);
            //向header头中赋值
            setView1(view, cinemaSubItem);
            listView.addHeaderView(view);

            view = LayoutInflater.from(this).inflate(R.layout.item_cinema_comment_2, null, false);
            setView2(view, cinemaSubItem);
            listView.addHeaderView(view);


            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);
            listView.setOnScrollListener(this);

            Drawable drawable = getResources().getDrawable(R.drawable.arrow_down);
            refreshListView.setLoadingDrawable(drawable);
            // 设置加载中的标题
            refreshListView.setRefreshingLabel("加载...");
            // 设置“松开”提醒
            refreshListView.setReleaseLabel("松开可以刷新");
            DateFormat dateFormat = SimpleDateFormat.getDateInstance();
            String s = dateFormat.format(new Date());

            refreshListView.setLastUpdatedLabel(s);

            refreshListView.setOnRefreshListener(this);

            pageCount = 1;
            commentId = cinemaSubItem.getId();
            loadData(Constants.getMovieCommentURL(null, commentId, 1, "desc", 10));


        }
    }

    //解析表情
    private void loadFaceData(String movieAllFace) {
        load_layout.setVisibility(View.VISIBLE);
        HttpUtils httpUtils = new HttpUtils();
        httpUtils.send(HttpRequest.HttpMethod.GET,
                movieAllFace,
                new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> objectResponseInfo) {
                        String result = objectResponseInfo.result;
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray faces = jsonObject.getJSONArray("faces");
                            for (int i = 0; i < faces.length(); i++) {
                                JSONObject object = faces.getJSONObject(i);
                                Face face = new Face();
                                face.parseJSON(object);
                                Constants.faceList.add(face);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(CinemaCommentActivity.this, "网络问题", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    //加载评论数据
    private void loadData(String url) {
        load_layout.setVisibility(View.VISIBLE);
        HttpUtils httpUtils = new HttpUtils();
        httpUtils.send(HttpRequest.HttpMethod.GET,
                url,
                new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> objectResponseInfo) {
                        String result = objectResponseInfo.result;
                        try {
                            refreshListView.setMode(PullToRefreshBase.Mode.BOTH);
                            refreshListView.onRefreshComplete();
                            JSONObject object = new JSONObject(result);
                            int total_count = object.getInt("total_count");
                            int total_pages = object.getInt("total_pages");
                            if (pageCount > total_pages) {
                                load_layout.setVisibility(View.GONE);
                                image_error.setVisibility(View.GONE);
                                return;
                            }
                            JSONArray comments = object.getJSONArray("comments");
                            for (int i = 0; i < comments.length(); i++) {
                                JSONObject jsonObject = comments.getJSONObject(i);
                                CinemaComment cinemaComment = new CinemaComment();
                                cinemaComment.parseJSON(jsonObject);
                                if (!cinemaCommentList.contains(cinemaComment)) {
                                    cinemaCommentList.add(cinemaComment);
                                }
                            }
                            Log.i("CinemaCommentActivity", cinemaCommentList.size() + "");
                            adapter.notifyDataSetChanged();
                            load_layout.setVisibility(View.GONE);
                            image_error.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(CinemaCommentActivity.this, "网络问题", Toast.LENGTH_SHORT).show();
                        image_error.setVisibility(View.VISIBLE);
                        refreshListView.setVisibility(View.GONE);
                        load_layout.setVisibility(View.GONE);
                        pageCount = 1;
                        refreshListView.setMode(PullToRefreshBase.Mode.DISABLED);
                    }
                });

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (position < listView.getHeaderViewsCount()) {
            return;
        } else {
            int location = position - listView.getHeaderViewsCount();
            CinemaComment cinemaComment = cinemaCommentList.get(location);
            if (this.position != location) {
                removeOperator(location);
            }
            this.position = location;

            allOperator(view, cinemaComment);
        }
    }

    private void allOperator(View view, CinemaComment cinemaComment) {
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.item_cinema_comment_operator);
        LinearLayout subLayout = (LinearLayout) view.findViewById(R.id.item_cinema_comment_sub);
        LayoutInflater inflater = LayoutInflater.from(this);

        int cinemaCommentId = cinemaComment.getId();

        int type = cinemaComment.getType();
        lastType = type;
        if (lastId != cinemaCommentId) {
//            addOperator(layout, inflater, cinemaComment);
//            removeView();
            lastId = cinemaCommentId;
        }

        addSubView(layout, subLayout, inflater, cinemaComment);
        adapter.notifyDataSetChanged();
    }

    /**
     * 向点击条目中添加子条目
     *
     * @param layout
     * @param subLayout
     * @param inflater
     * @param cinemaComment
     */
    private void addSubView(LinearLayout layout, LinearLayout subLayout, LayoutInflater inflater, CinemaComment cinemaComment) {
        List<CinemaComment> subComments = cinemaComment.getSubComments();
        if (subComments.size() > 0) {
            if (!cinemaComment.isOpen()) {
                cinemaComment.setOpen(true);

                for (int i = 0; i < subComments.size(); i++) {
                    View subView = inflater.inflate(R.layout.item_cinema_comment_32, layout, false);
                    CinemaComment subComment = subComments.get(i);
                    subView.setTag(subComment);
                    subView.setOnClickListener(this);
                    TextView userName = (TextView) subView.findViewById(R.id.item_cinema_comment_32_userName);
                    TextView time = (TextView) subView.findViewById(R.id.item_cinema_comment_32_time);
                    ImageView imageView = (ImageView) subView.findViewById(R.id.item_cinema_comment_32_image);
                    TextView content = (TextView) subView.findViewById(R.id.item_cinema_comment_32_content);

                    User user = subComment.getUser();
                    userName.setText(user.getLogin());
                    time.setText(subComment.getCreated_at());

                    content.setMovementMethod(LinkMovementMethod.getInstance());
                    Html.ImageGetter imageGetter = new URLImageGetter(this, content);
                    Spanned spanned = Html.fromHtml(subComment.getContent(), imageGetter, null);
                    content.setText(spanned);

                    List<CinemaComment> subComments1 = subComment.getSubComments();
                    if (subComments1.size() > 0) {
                        //代表未展开，闭合
                        imageView.setTag(true);
                        imageView.setImageResource(R.drawable.show_children);


                    }
                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    params1.setMargins(subComment.getType() * 15, 15, 0, 0);
                    subView.setLayoutParams(params1);
                    subLayout.addView(subView);
                }
            } else {
                cinemaComment.setOpen(false);
                subLayout.removeAllViews();
                layout.removeAllViews();
            }
        }
    }

    private void removeOperator(int location) {
        for (int i = 0; i < cinemaCommentList.size(); i++) {
            if (i != location) {
                View view = listView.getChildAt(i);
                if (view != null) {

                    TextView textView = (TextView) view.findViewById(R.id.item_cinema_comment_3_userName);
                    if (textView != null) {
                        System.out.println("textView.getText().toString() = " + textView.getText().toString());
                    }
                    LinearLayout operator = (LinearLayout) view.findViewById(R.id.item_cinema_comment_operator);

                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.item_cinema_comment_sub);
                    if (operator != null && layout != null) {
                        layout.removeAllViews();
                        operator.removeAllViews();
                    }
                    CinemaComment cinemaComment = cinemaCommentList.get(i);
                    cinemaComment.setOpen(false);
                }
            }
        }


    }

    /**
     * 点赞，踩，评论，回复，举报   的动态添加
     *
     * @param layout
     * @param inflater
     * @param cinemaComment
     */
    private void addOperator(LinearLayout layout, LayoutInflater inflater, CinemaComment cinemaComment) {
        View operatorView = inflater.inflate(R.layout.item_operator, layout, false);
        View ding = operatorView.findViewById(R.id.item_cinema_comment_3_ding);
        if (ding != null) {
            ding.setTag(cinemaComment);
            ding.setOnClickListener(this);

        }
        View cai = operatorView.findViewById(R.id.item_cinema_comment_3_cai);
        if (cai != null) {
            cai.setTag(cinemaComment);
            cai.setOnClickListener(this);
        }
        View reply = operatorView.findViewById(R.id.item_cinema_comment_3_reply);
        if (reply != null) {
            reply.setTag(cinemaComment);
            reply.setOnClickListener(this);
        }
        View jubao = operatorView.findViewById(R.id.item_cinema_comment_3_jubao);
        if (jubao != null) {
            jubao.setTag(cinemaComment);
            jubao.setOnClickListener(this);
        }
        View share = operatorView.findViewById(R.id.item_cinema_comment_3_share);
        if (share != null) {
            share.setTag(cinemaComment);
            share.setOnClickListener(this);
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        operatorView.setLayoutParams(params);
        layout.addView(operatorView);
    }

    @Override
    public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String url = Constants.getMovieCommentURL(null, commentId, 1, type, 10);
                pageCount = 1;
                loadData(url);
                refreshListView.setMode(PullToRefreshBase.Mode.BOTH);
            }
        }, 1000);
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pageCount++;
                String url = Constants.getMovieCommentURL(null, commentId, pageCount, type, 10);
                loadData(url);
            }
        }, 1000);

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        switch (id) {
            case R.id.cinemaList_image_error:
                image_error.setVisibility(View.GONE);
                loadData(Constants.MOVIE_INDEX_URL);
                break;
            case R.id.item_cinema_comment_3_ding:
                executeTask(v, new DingTask(this));
                break;
            case R.id.item_cinema_comment_3_cai:
                executeTask(v, new CaiTask(this));
                break;
            case R.id.item_cinema_comment_3_reply:
                Toast.makeText(this, "ding", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_cinema_comment_3_share:
                Toast.makeText(this, "ding", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_cinema_comment_3_jubao:
                executeTask(v, new ReportTask(this));
                break;
            case R.id.item_cinema_comment_1_play:
                Intent intent = new Intent(this, TopicPagerMovieActivity.class);
                intent.putExtra("mp4_file_link", this.cinemaSubItem.getMp4_file_link());
                startActivity(intent);
                break;
            case R.id.item_cinema_comment_3_userName:
                //TODO 页面跳转
                intent = new Intent(this, UserDetailActivity.class);
                int pos = (int) v.getTag();
                CinemaComment cinemaComment = cinemaCommentList.get(pos);
                User user = cinemaComment.getUser();
                intent.putExtra("user", user);
                startActivity(intent);
                break;
            case R.id.item_cinema_comment_1_share:
                showShare(this.cinemaSubItem.getMp4_file_link());
                break;
            case R.id.comment_32_layout:
                ImageView imageView = (ImageView) v.findViewById(R.id.item_cinema_comment_32_image);
                if (imageView != null) {
                    Boolean b = (Boolean) imageView.getTag();
                    if (b != null) {
                        if (b) {
                            //代表闭合，进行展开操作
                            imageView.setImageResource(R.drawable.close_children);
                            imageView.setTag(false);
                        } else {
                            imageView.setImageResource(R.drawable.show_children);
                            imageView.setTag(true);
                        }
                    }
                }
                cinemaComment = (CinemaComment) v.getTag();
                allOperator(v, cinemaComment);
                break;
        }
    }

    /**
     * 执行赞，踩，评论，回复，举报的异步任务
     *
     * @param v
     * @param task
     */
    private void executeTask(View v, AsyncTask task) {
        LoginUser loginUser = LoginUtils.getLoginUser();
        if (loginUser != null) {
            CinemaComment cinemaComment = (CinemaComment) v.getTag();

            if (task instanceof DingTask) {
                DingTask dingTask = (DingTask) task;
                dingTask.execute(
                        Integer.toString(cinemaComment.getArticle_id()),
                        Integer.toString(cinemaComment.getId()),
                        Integer.toString(loginUser.getUser_id()),
                        loginUser.getAccess_token());

                if (cinemaComment.isMark()) {
                    imageFinish.setVisibility(View.VISIBLE);
                    imageFinish.startAnimation(animation);
                } else {
                    imageDing.setVisibility(View.VISIBLE);
                    imageDing.startAnimation(animation);
                    cinemaComment.setMark(true);
                    cinemaComment.setDing(true);
                }
            } else if (task instanceof CaiTask) {
                CaiTask caiTask = (CaiTask) task;
                caiTask.execute(
                        Integer.toString(cinemaComment.getArticle_id()),
                        Integer.toString(cinemaComment.getId()),
                        Integer.toString(loginUser.getUser_id()),
                        loginUser.getAccess_token());

                if (cinemaComment.isMark()) {
                    imageFinish.setVisibility(View.VISIBLE);
                    imageFinish.startAnimation(animation);
                } else {
                    imageCai.setVisibility(View.VISIBLE);
                    imageCai.startAnimation(animation);
                    cinemaComment.setMark(true);
                    cinemaComment.setCai(true);
                }
            } else if (task instanceof ReportTask) {
                ReportTask reportTask = (ReportTask) task;
                reportTask.execute(
                        Integer.toString(cinemaComment.getId()),
                        Integer.toString(loginUser.getUser_id()),
                        loginUser.getAccess_token());
            }
            adapter.notifyDataSetChanged();
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    /**
     * ShareSdk 第三方分享功能测试
     */
    private void showShare(String mp4Path) {
        //这个方法必须调用，初始化ShareSDK
        ShareSDK.initSDK(this);
        //一键分享的代码
        OnekeyShare oks = new OnekeyShare();
        // 分享时Notification的图标和文字
        oks.setNotification(R.drawable.caocaocao, getString(R.string.app_name));
        //分享的文字文本
        oks.setText("暴走精选视频：" + mp4Path);
        oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/05/21/oESpJ78_533x800.jpg");
        // 启动分享GUI

        oks.show(this);
    }

    //向ListView 的头中添加View1
    private void setView1(View view, CinemaSubItem cinemaSubItem) {
        ImageView image = (ImageView) view.findViewById(R.id.item_cinema_comment_1_image);
        ImageView icon = (ImageView) view.findViewById(R.id.item_cinema_comment_1_icon);
        TextView title = (TextView) view.findViewById(R.id.item_cinema_comment_1_title);
        TextView desc = (TextView) view.findViewById(R.id.item_comment_1_desc);
        TextView ding = (TextView) view.findViewById(R.id.item_cinema_comment_1_ding);
        TextView comment1 = (TextView) view.findViewById(R.id.item_cinema_comment_1_comment1);
        TextView comment2 = (TextView) view.findViewById(R.id.item_cinema_comment_1_comment2);
        LinearLayout share = (LinearLayout) view.findViewById(R.id.item_cinema_comment_1_share);
        share.setOnClickListener(this);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.item_cinema_comment_1_play);
        CardView cardView = (CardView) view.findViewById(R.id.item_cinema_comment_1_cardView);
//        cardView.setOnTouchListener(t);
        layout.setOnClickListener(this);
        if (cinemaSubItem != null) {
            title.setText(cinemaSubItem.getUser_login());
            desc.setText(cinemaSubItem.getTitle());
            ding.setText(cinemaSubItem.getPos() + "");
            comment1.setText(cinemaSubItem.getPublic_comments_count() + "");
            comment2.setText(cinemaSubItem.getPublic_comments_count() + "条评论");
            String imageLink = cinemaSubItem.getMp4_image_link();
            if (imageLink != null) {
                BitmapDisplayConfig config = new BitmapDisplayConfig();
                // config.setShowOriginal(true); // 显示原始图片,不压缩, 尽量不要使用,
                // 图片太大时容易OOM。
                config.setBitmapConfig(Bitmap.Config.RGB_565);
                config.setBitmapMaxSize(new BitmapSize(117, 66));
                System.out.println("imageLink = " + imageLink);
                BitmapHelp.getUtils().display(image, imageLink, config);
            }
            String small_pictures = cinemaSubItem.getSmall_pictures();
            if (small_pictures != null) {
                BitmapDisplayConfig config = new BitmapDisplayConfig();
                //config.setShowOriginal(true); // 显示原始图片,不压缩, 尽量不要使用,
                // 图片太大时容易OOM。
                config.setBitmapConfig(Bitmap.Config.RGB_565);
                config.setBitmapMaxSize(new BitmapSize(50, 50));
                System.out.println("imageLink = " + imageLink);
                BitmapHelp.getUtils().display(icon, small_pictures, config);
            }
        }
    }

    //向ListView 的头中添加View2
    private void setView2(View view, CinemaSubItem cinemaSubItem) {
        TextView comment = (TextView) view.findViewById(R.id.item_cinema_comment_2_comment);
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.item_cinema_comment_2_radioGroup);
        radioGroup.setOnCheckedChangeListener(this);
        if (radioGroup != null) {
            View v = radioGroup.getChildAt(0);
            if (v instanceof RadioButton) {
                RadioButton rb = (RadioButton) v;
                rb.setChecked(true);
            }
        }
        comment.setText(cinemaSubItem.getPublic_comments_count() + "条评论");
    }

    /**
     * 最新，最热，最高分 的点击事件
     *
     * @param group
     * @param checkedId
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int count = group.getChildCount();
        int position = -1;
        for (int i = 0; i < count; i++) {
            View view = group.getChildAt(i);
            if (view instanceof RadioButton) {
                RadioButton rb = (RadioButton) view;
                if (rb.isChecked()) {
                    position = i;
                    break;
                }
            }
        }
        if (position != -1) {
            cinemaCommentList.clear();
            switch (position) {
                case 0:
                    loadData(Constants.getMovieCommentURL(null, commentId, 1, "desc", 10));
                    type = "desc";
                    break;
                case 1:
                    loadData(Constants.getMovieCommentURL(null, commentId, 1, "new", 10));
                    type = "new";
                    break;
                case 2:
                    loadData(Constants.getMovieCommentURL(null, commentId, 1, "best", 10));
                    type = "best";
                    break;
                case 3:
                    loadData(Constants.getMovieCommentURL(null, commentId, 1, "high", 10));
                    type = "high";
                    break;
            }
        }
    }

   /* private void setFlags(CinemaComment cinemaComment, List<CinemaComment> data) {
        for (CinemaComment c : data) {
            if (c == cinemaComment) {
                if (cinemaComment.isFlag()) {
                    cinemaComment.setFlag(false);
                } else {
                    cinemaComment.setFlag(true);
                }
                break;
            } else {
                c.setFlag(false);
            }
            *//*List<CinemaComment> subComments = c.getSubComments();
            for (CinemaComment comment : subComments) {
                setFlags(cinemaComment, subComments);
            }*//*
        }
    }*/

    /////////////////////////动画处理///////////////////////////////////////////////////////////////////
    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (imageDing.getVisibility() == View.VISIBLE) {
            imageDing.setVisibility(View.GONE);
        }
        if (imageCai.getVisibility() == View.VISIBLE) {
            imageCai.setVisibility(View.GONE);
        }
        if (imageFinish.getVisibility() == View.VISIBLE) {
            imageFinish.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    /**
     * 赞，踩，评论。。。异步任务的返回结果
     *
     * @param jsonObject
     */
    @Override
    public void backResult(JSONObject jsonObject) {
        if (jsonObject != null) {
            System.out.println("jsonObject = " + jsonObject);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    private int firstVisibleItem;
    private int visibleItemCount;

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.firstVisibleItem = firstVisibleItem;
        this.visibleItemCount = visibleItemCount;
    }
}

