package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by zhangkx on 2015/3/10.
 */
public class  User implements Serializable {


    /*  "user": {
  "id": 6775725,
          "login": "\u66f9\u8001\u4e8c",
          "avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/0/original/missing.png"
},*/
    private String login;
    private String avatar;
    private String id;

    public User() {
    }

    public User(String login, String avatar, String id) {
        this.login = login;
        this.avatar = avatar;
        this.id = id;
    }

    /**
     * 解析JSON数据
     *
     * @param json
     */
    public void parseJSON(JSONObject json) {
        if (json != null) {

            try {
                login = json.getString("login"); // 必须存在的字段优先解析，这样保证数据的有效性
                avatar = json.getString("avatar");
                id = json.getString("id");


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
