package com.qianfeng.android.baoman.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by ldy on 2015/3/11.
 */
public class FullGridView extends GridView {
    public FullGridView(Context context) {
        super(context);
    }

    public FullGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // 获取宽度规格中的宽度数值
        int wSize = MeasureSpec.getSize(widthMeasureSpec);
        int wMode = MeasureSpec.getMode(widthMeasureSpec);

        // 获取宽度规格中的宽度数值
        int hSize = MeasureSpec.getSize(heightMeasureSpec);
        int hMode = MeasureSpec.getMode(heightMeasureSpec);


        // mode 代表了，尺寸计算的方式。
        // MeasureSpec 里面提供了尺寸计算模式的常量定义

        switch (hMode){
            case MeasureSpec.AT_MOST: // wrap_content
                break;
            case MeasureSpec.EXACTLY: // match_parent, xxxdp
                break;
            case MeasureSpec.UNSPECIFIED: // 未指定，不限定尺寸计算的方式。
                break;
        }

        // 如果我们能够，把高度 hSize 设置一个很大的值，
        // 并且将尺寸计算模式，调整为AT_MOST(wrap_content)
        // 那么GridView将能够完全显示

        // 重新生成一个 int值，这个值通过MeasureSpec将 size 和 mode 组合
        // 就可以放GridView放大了

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 1, MeasureSpec.AT_MOST);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
