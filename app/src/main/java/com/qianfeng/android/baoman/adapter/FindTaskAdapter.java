package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.Task;

import java.util.List;

/**
 * Created by ldy on 2015/3/10.
 */
public class FindTaskAdapter extends BaseAdapter{

    private List<Task> tasks;
    private LayoutInflater inflater;

    public FindTaskAdapter(Context context,List<Task> tasks) {
        if (context != null) {
            inflater = LayoutInflater.from(context);
        }else {
            throw new IllegalArgumentException("Context must be not null");
        }

        this.tasks = tasks;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if (tasks != null) {
            ret = tasks.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int position) {
        Object ret = null;
        if (tasks != null) {
            ret = tasks.get(position);
        }
        return ret;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;
        if (convertView != null) {
            ret = convertView;
        }else {
            ret = inflater.inflate(R.layout.item_task_find,parent,false);
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.headIcon = (ImageView) ret.findViewById(R.id.find_task_item_icon);
            holder.NiMoney = (TextView) ret.findViewById(R.id.find_task_item_nimabi_txt);
            holder.title = (TextView) ret.findViewById(R.id.find_task_item_title);
            holder.description = (TextView) ret.findViewById(R.id.find_task_item_description);
            holder.btnGoto = (Button) ret.findViewById(R.id.find_task_item_btn);

            ret.setTag(holder);
        }


        Task task = tasks.get(position);

        //显示任务标题
        String taskName = task.getName();
        holder.title.setText(taskName);

        //显示任务描述
        String taskDesc = task.getDesc();
        holder.description.setText(taskDesc);

        //显示尼玛币
        String award = task.getStage().getAward();
        holder.NiMoney.setText(award+"尼");

        //TODO 加载图片
        //设置默认图片，避免出现复用的View显示以前的图片的问题
        holder.headIcon.setImageResource(R.drawable.clean_default);
        String icon = task.getIcon();
        String end_icon = task.getEnd_icon();
        holder.headIcon.setTag(icon);

        BitmapHelp.getUtils().display(holder.headIcon,task.getIcon(),new BitmapLoadCallBack<ImageView>() {
            @Override
            public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

            }
        });
        return ret;
    }

    private static class ViewHolder{
        private ImageView headIcon;  //任务的图标
        private TextView NiMoney;   //尼玛币数量
        private TextView title;   //任务标题
        private TextView description;   //任务描述
        private Button btnGoto;
    }
}
