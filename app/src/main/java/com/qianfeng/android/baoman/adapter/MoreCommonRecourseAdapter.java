package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.RecourseList;

import java.util.List;

/**
 * Created by ldy on 2015/3/13.
 */
public class MoreCommonRecourseAdapter extends BaseAdapter {
    private List<RecourseList> recourseListList;
    private LayoutInflater inflater;



    public MoreCommonRecourseAdapter(Context context, List<RecourseList> recourseListList) {
        if (context != null) {
            inflater = LayoutInflater.from(context);
        }else {
            throw new IllegalArgumentException("Context must be not null");
        }

        this.recourseListList = recourseListList;
    }



    @Override
    public int getCount() {
        int ret = 0;
        if (recourseListList != null) {
            ret = recourseListList.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int position) {
        Object ret = null;
        if (recourseListList != null) {
            ret = recourseListList.get(position);
        }
        return ret;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;

        if (convertView != null) {
            ret = convertView;
        }else {
            ret = inflater.inflate(R.layout.item_more_recourse_find,parent,false);
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.cartoonImg = (ImageView) ret.findViewById(R.id.find_recourse_more_common_img);
            holder.state = (TextView) ret.findViewById(R.id.find_recourse_more_common_state);
            holder.title = (TextView) ret.findViewById(R.id.find_recourse_more_common_title);
            holder.description = (TextView) ret.findViewById(R.id.find_recourse_more_common_description);
            holder.isFavourited = (TextView) ret.findViewById(R.id.find_recourse_more_common_favorite);
            holder.heart = (ImageView) ret.findViewById(R.id.find_recourse_more_common_heart);
            holder.update = (TextView) ret.findViewById(R.id.find_recourse_more_common_update);

            ret.setTag(holder);
        }

        RecourseList recourseList = recourseListList.get(position);

        //显示图片
        holder.cartoonImg.setImageResource(R.drawable.maker_default_face_1);
        String pic = recourseList.getPic();
        holder.cartoonImg.setTag(pic);

        BitmapHelp.getUtils().display(holder.cartoonImg,recourseList.getPic(),new BitmapLoadCallBack<ImageView>() {
            @Override
            public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

            }
        });



        //显示标题
        String name = recourseList.getName();
        holder.title.setText(name);

        //显示状态
        String list_type = recourseList.getList_type();
        holder.state.setText(list_type);

        //显示描述
        String description = recourseList.getDescription();
        holder.description.setText(description);


        //显示是否收藏    //显示收藏（心）颜色
        String favorited = recourseList.getFavorited();
        if ("false".equals(favorited)){
            holder.isFavourited.setText("收藏");
            holder.heart.setImageResource(R.drawable.comic_book_collect);
        }else {
            holder.isFavourited.setText("已收藏");
            holder.heart.setImageResource(R.drawable.comic_book_collected);
        }

        //显示更新至第几话
        String lastUpdate = recourseList.getLatestInfo().getName();
        holder.update.setText("更新至："+lastUpdate);





        return ret;
    }

    private static class ViewHolder{
        private ImageView cartoonImg;  //漫画的图片
        private TextView state;   //漫画的状态
        private TextView title;   //漫画标题
        private TextView description;   //漫画描述
        private TextView update;   //漫画更新
        private TextView isFavourited;  //漫画是否收藏
        private ImageView heart;   // 收藏的心形图标

    }
}
