package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Android_Studio on 2015/3/10.
 * User:Maxiaoyu
 * Date:2015/3/10
 * Email:731436452@qq.com
 */
public class CinemaItem implements Serializable {
//    "index": 229,
//            "media_url": "http://hc25.aipai.com/user/596/17601596/4590714/card/24726465/card.mp4"

    private int index;
    private String media_url;

    public void parseJSON(JSONObject jsonObject){
        if (jsonObject != null) {
            try {
                index=jsonObject.getInt("index");
                media_url=jsonObject.getString("media_url");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }
}
