package com.qianfeng.android.baoman.task;

import android.os.AsyncTask;

import com.qianfeng.android.baoman.util.HttpTool;

import java.util.HashMap;

/**
 * Created by Android_Studio on 2015/3/16.
 * User:Maxiaoyu
 * Date:2015/3/16
 * Email:731436452@qq.com
 */
public class BindLoginTask extends AsyncTask<HashMap<String, String>, Integer, String> {

    @Override
    protected String doInBackground(HashMap<String, String>... params) {
        String ret = null;
        if (params != null) {
            String str = "http://api.ibaozou.com/api/v2/push/install";
            byte[] bytes = HttpTool.post(str, params[0]);
            if (bytes != null) {
                ret = new String(bytes, 0, bytes.length);
            }
        }
        return ret;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null) {
            System.out.println("s = " + s);
        }
    }
}
