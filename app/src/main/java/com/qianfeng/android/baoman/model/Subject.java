package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by ldy on 2015/3/12.
 */
public class Subject implements Serializable{
    /*
    {
        private String id;//34,
        private String cover_path;//"http://hahabaozou.qiniudn.com/new-subjects/2014/10/1412732934003-q0irmlr3xkygg8sobak8ol-e83dea85f4973a1372430b0d9c9a4395-欧美坑.jpg",
        private String name;//"必看美漫",        //专题名称
        private String info;//"在追剧么？在等电影么？接下来的剧情都在这里呦。好奇不？心痒不？警告：欧美坑慎入哦。",
        private String publish_time;//"2014-10-08T09:43:00+08:00",
        private String created_at;//"2014-10-08T09:48:20+08:00",
        private String updated_at;//"2014-10-13T16:52:33+08:00"
    }

    ,.....
        "total_pages":2,
        "page":1,
        "pagesize":10,
        "total_count":15
       */

    private String id;  //34,
    private String cover_path;  //"http://hahabaozou.qiniudn.com/new-subjects/2014/10/1412732934003-q0irmlr3xkygg8sobak8ol-e83dea85f4973a1372430b0d9c9a4395-欧美坑.jpg",
    private String name;  //"必看美漫",        //专题名称
    private String info;   //"在追剧么？在等电影么？接下来的剧情都在这里呦。好奇不？心痒不？警告：欧美坑慎入哦。",
    private String publish_time;  //"2014-10-08T09:43:00+08:00",
    private String created_at;  //"2014-10-08T09:48:20+08:00",
    private String updated_at;   //"2014-10-13T16:52:33+08:00"

    //解析Json数据
    public void parseJSON(JSONObject json){
        if (json != null) {
            try {

                id = json.getString("id");
                name = json.getString("name");
                info = json.getString("info");
                publish_time = json.getString("publish_time");
                created_at = json.getString("created_at");
                updated_at = json.getString("updated_at");


                //利用正则解决请求中有中文的问题
                cover_path = json.getString("cover_path");

                int temp1=cover_path.lastIndexOf("-");
                int temp2=cover_path.lastIndexOf(".");
                String oldString = cover_path.substring(temp1+1, temp2);

                String newString = URLEncoder.encode(oldString, "utf-8");

                cover_path = cover_path.replace(oldString,newString);





            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCover_path() {
        return cover_path;
    }

    public void setCover_path(String cover_path) {
        this.cover_path = cover_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPublish_time() {
        return publish_time;
    }

    public void setPublish_time(String publish_time) {
        this.publish_time = publish_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
