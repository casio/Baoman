package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.WebMaker;

import java.util.List;

/**
 * Created by ldy on 2015/3/12.
 */
public class FindWebMarkerAdapter extends BaseAdapter implements View.OnClickListener {

    private List<WebMaker> webMakers;
    private LayoutInflater inflater;

    public FindWebMarkerAdapter(Context context,List<WebMaker> webMakers) {
        if (context != null) {
            inflater = LayoutInflater.from(context);
        }else {
            throw new IllegalArgumentException("Context must be not null");
        }

        this.webMakers = webMakers;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if (webMakers != null) {
            ret = webMakers.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int i) {
        Object ret = null;
        if (webMakers != null) {
            ret = webMakers.get(i);
        }
        return ret;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;
        if (convertView != null) {
            ret = convertView;
        }else {
            ret = inflater.inflate(R.layout.item_web_marker_find,parent,false);
        }


        ViewHolder holder = (ViewHolder) ret.getTag();
        if(holder == null){
            holder = new ViewHolder();
            holder.webMarkerImg = (ImageView) ret.findViewById(R.id.find_web_marker_img);
            holder.webMarkerTitle = (TextView) ret.findViewById(R.id.find_web_marker_title);
            holder.webMarkerDescribe = (TextView) ret.findViewById(R.id.find_web_marker_describe);
            holder.webMarkerGo = (ImageView) ret.findViewById(R.id.find_web_marker_go);

            ret.setTag(holder);
        }


        WebMaker webMaker = webMakers.get(position);

        //设置标题
        String name = webMaker.getName();
        holder.webMarkerTitle.setText(name);

        //设置描述
        String desc = webMaker.getDesc();
        holder.webMarkerDescribe.setText(desc);

        //设置图标
        String icon = webMaker.getIcon();
        BitmapHelp.getUtils().display(holder.webMarkerImg, webMaker.getIcon(),new BitmapLoadCallBack<ImageView>() {
            @Override
            public void onLoadCompleted(ImageView imageView, String s, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, BitmapLoadFrom bitmapLoadFrom) {
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadFailed(ImageView imageView, String s, Drawable drawable) {

            }
        });

        //go 点击事件处理
        holder.webMarkerGo.setOnClickListener(this);


        return ret;
    }

    @Override
    public void onClick(View view) {

    }


    public static class ViewHolder{

        private ImageView webMarkerImg;
        private TextView webMarkerTitle;
        private TextView webMarkerDescribe;
        private ImageView webMarkerGo;

    }
}
