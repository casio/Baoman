package com.qianfeng.android.baoman.model;

import android.util.Log;

import com.qianfeng.android.baoman.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Android_Studio on 2015/3/12.
 * User:Maxiaoyu
 * Date:2015/3/12
 * Email:731436452@qq.com
 */
public class CinemaComment implements Serializable {
    private int id;//26490454
    private int article_id;//17356800
    private int pos;//85
    private int neg;//1
    private int floor;//333
    private String client_type;//"\u66b4\u8d70\u6f2b\u753bAndroid"
    private String status;//"publish"
    private int user_id;//6775725
    private String created_at;//"2015-03-11 12:17:51"
    private int score;//961925

    private boolean anonymous;//"anonymous": false,
    private String face_id;//""
    private int comment_user_id;//6775725
    private String content;//"\u662f\u5047\u7684\uff0c\u662f\u86ca\u866b\u7684\u7279\u6280\uff0c\u662f\u5316\u5b66\u7684\u6210\u5206"
    private String attachment;//""
    private String attachment_type;//""
    private String attachment_content_type;//""
    private User user;
    private int rated;//0

    //判断是第几层
    private int type = 0;


    private boolean isDing;
    private boolean isCai;
    private boolean isOpen=false;
    //标记是否被踩或者顶
    private boolean isMark = false;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isMark() {
        return isMark;
    }

    public void setMark(boolean isMark) {
        this.isMark = isMark;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public boolean isDing() {
        return isDing;
    }

    public void setDing(boolean isDing) {
        this.isDing = isDing;
    }

    public boolean isCai() {
        return isCai;
    }

    public void setCai(boolean isCai) {
        this.isCai = isCai;
    }

    @Override
    public boolean equals(Object o) {
        CinemaComment comment = null;
        if (o instanceof CinemaComment) {
            comment = (CinemaComment) o;
        }
        return this.created_at.equals(comment.getCreated_at());
    }

    private boolean flag = false;

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    private List<CinemaComment> subComments;

    public CinemaComment() {
        subComments = new LinkedList<>();
    }

    public void parseJSON(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");// 26490454,
            article_id = jsonObject.getInt("article_id");// 17356800,
            pos = jsonObject.getInt("pos");// 85,
            neg = jsonObject.getInt("neg");// 1,
            floor = jsonObject.getInt("floor");// 333,
            client_type = jsonObject.getString("client_type");// "\u66b4\u8d70\u6f2b\u753bAndroid",
            status = jsonObject.getString("status");// "publish",
            user_id = jsonObject.getInt("user_id");// 6775725,
            created_at = jsonObject.getString("created_at");// "2015-03-11 12:17:51",
            score = jsonObject.getInt("score");// 961925,
            anonymous = jsonObject.getBoolean("anonymous");//"anonymous": false,
            face_id = jsonObject.optString("face_id");// "",
            comment_user_id = jsonObject.getInt("comment_user_id");// 6775725,
            content = jsonObject.getString("content");// "\u662f\u5047\u7684\uff0c\u662f\u86ca\u866b\u7684\u7279\u6280\uff0c\u662f\u5316\u5b66\u7684\u6210\u5206",
            content = getFaceUrl(content);
            attachment = jsonObject.optString("attachment");// "",
            attachment_type = jsonObject.optString("attachment_type");// "",
            attachment_content_type = jsonObject.optString("attachment_content_type");// "",
                  /*  "user": {
                "id": 6775725,
                        "login": "\u66f9\u8001\u4e8c",
                        "avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/0/original/missing.png"
            },*/
            type = type + 1;
            if (type >= 4) {
                type = 4;
            }
            user = new User();
            user.parseJSON(jsonObject.getJSONObject("user"));
            rated = jsonObject.getInt("rated");// 0,
            JSONArray children = jsonObject.getJSONArray("children");
            for (int i = 0; i < children.length(); i++) {
                JSONObject object = children.getJSONObject(i);
                CinemaComment cinemaComment = new CinemaComment();
                cinemaComment.parseJSON(object);
                subComments.add(cinemaComment);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getFaceUrl(String content) {
        Log.i("CinemaComment:content", content);
        Pattern p = Pattern.compile("\\[face\\d+\\]");
        Matcher m = p.matcher(content);
        while (m.find()) {
            String group = m.group();
            Log.i("CinemaComment:group", group);
            for (Face f : Constants.faceList) {
                String code = f.getCode();
                String str = "[" + code + "]";
                if (str.equals(group)) {
                    Log.i("CinemaComment:code", code + ":" + group);
                    content = content.replaceFirst(code, "<img src='" + f.getUrl() + "'/>");
                    content = content.replaceFirst("\\[", "");
                    content = content.replaceFirst("\\]", "");
                }
            }
        }

        Log.i("CinemaComment", content);
        return content;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getNeg() {
        return neg;
    }

    public void setNeg(int neg) {
        this.neg = neg;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getClient_type() {
        return client_type;
    }

    public void setClient_type(String client_type) {
        this.client_type = client_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public String getFace_id() {
        return face_id;
    }

    public void setFace_id(String face_id) {
        this.face_id = face_id;
    }

    public int getComment_user_id() {
        return comment_user_id;
    }

    public void setComment_user_id(int comment_user_id) {
        this.comment_user_id = comment_user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachment_type() {
        return attachment_type;
    }

    public void setAttachment_type(String attachment_type) {
        this.attachment_type = attachment_type;
    }

    public String getAttachment_content_type() {
        return attachment_content_type;
    }

    public void setAttachment_content_type(String attachment_content_type) {
        this.attachment_content_type = attachment_content_type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getRated() {
        return rated;
    }

    public void setRated(int rated) {
        this.rated = rated;
    }

    public List<CinemaComment> getSubComments() {
        return subComments;
    }

    public void setSubComments(List<CinemaComment> subComments) {
        this.subComments = subComments;
    }
}
