package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ldy on 2015/3/10.
 */
public class Stage {

    private String name;//"name":"顶你喜欢的10篇神作",    //任务名称
    private String award;//"award":"10",     奖励的尼币
    private String count;//"count":"10",
    private String id;//"id":0,
    private String receive_status;//"receive_status":0,
    private String current_count;//"current_count":0,
    private String gotoHome; //"goto":"home"

    //解析Json数据
    public void parseJSON(JSONObject json){
        if (json != null) {
            try {
                name = json.getString("name");
                award = json.getString("award");
                count = json.getString("count");
                id = json.getString("id");
                receive_status = json.getString("receive_status");
                current_count = json.getString("current_count");
                gotoHome = json.getString("goto");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAward() {
        return award;
    }

    public void setAward(String award) {
        this.award = award;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceive_status() {
        return receive_status;
    }

    public void setReceive_status(String receive_status) {
        this.receive_status = receive_status;
    }

    public String getCurrent_count() {
        return current_count;
    }

    public void setCurrent_count(String current_count) {
        this.current_count = current_count;
    }

    public String getGotoHome() {
        return gotoHome;
    }

    public void setGotoHome(String gotoHome) {
        this.gotoHome = gotoHome;
    }



}
