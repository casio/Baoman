package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.CartoonInfos;
import com.qianfeng.android.baoman.model.RecourseList;

import java.util.List;

/**
 * Created by ldy on 2015/3/16.
 */
public class CartoonItemDetailsAdapter extends BaseAdapter {
    private List<CartoonInfos> cartoonInfoses;
    private LayoutInflater inflater;



    public CartoonItemDetailsAdapter(Context context, List<CartoonInfos> cartoonInfoses) {
        if (context != null) {
            inflater = LayoutInflater.from(context);
        }else {
            throw new IllegalArgumentException("Context must be not null");
        }

        this.cartoonInfoses = cartoonInfoses;
    }



    @Override
    public int getCount() {
        int ret = 0;
        if (cartoonInfoses != null) {
            ret = cartoonInfoses.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int position) {
        Object ret = null;
        if (cartoonInfoses != null) {
            ret = cartoonInfoses.get(position);
        }
        return ret;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;

        if (convertView != null) {
            ret = convertView;
        }else {
            ret = inflater.inflate(R.layout.item_cartoon_details_num,parent,false);
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.itemPart = (TextView) ret.findViewById(R.id.cartoon_item_details_part);

            ret.setTag(holder);
        }

        CartoonInfos infos = cartoonInfoses.get(position);

        String name = infos.getName();
        holder.itemPart.setText(name);

        return ret;
    }

    private static class ViewHolder{
        private TextView itemPart;
    }
}
