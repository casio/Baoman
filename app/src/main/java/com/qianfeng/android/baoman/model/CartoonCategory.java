package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ldy on 2015/3/11.
 */
public class CartoonCategory {

    /*"category":{              //图书类型
        "name":"少年热血",
                "id":1
    },*/

    private String name;
    private String id;     //1代表 热门连载的id


    //解析Json数据
    public void parseJSON(JSONObject json) {
        if (json != null) {
            try {

                name = json.getString("name");
                id = json.getString("id");

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
