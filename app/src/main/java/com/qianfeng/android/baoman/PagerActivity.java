package com.qianfeng.android.baoman;

import android.os.Bundle;


public class PagerActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);

//初始化ActionBar
        setActionBarTitle("小纸条");
        setBackVisible(false);
        setFunctionVisible(false);
        //设置默认的RadioButton点击事件
        setChildRadioChecked(3);
    }


}
