package com.qianfeng.android.baoman.task;

import android.os.AsyncTask;
import android.util.Log;

import com.qianfeng.android.baoman.Constants;
import com.qianfeng.android.baoman.util.HttpTool;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by Android_Studio on 2015/3/17.
 * User:Maxiaoyu
 * Date:2015/3/17
 * Email:731436452@qq.com
 */

public class ReportTask extends AsyncTask<String, Integer, byte[]> {
    private TaskInterface listener;

    public ReportTask(TaskInterface listener) {
        this.listener = listener;
    }

    @Override
    protected byte[] doInBackground(String... params) {
        byte[] ret = null;
        if (params != null && params.length == 3) {
            String paramInt1 = params[0];
            String paramInt2 = params[1];
            String paramString = params[2];
            String str1 = Constants.getReportCommUrl(paramInt1);
            String str2 = Long.toString(System.currentTimeMillis() / 1000L);
            HashMap localHashMap = new HashMap();
            localHashMap.put("user_id", paramInt2);
            localHashMap.put("client_id", "10230158");
            localHashMap.put("access_token", paramString);
            localHashMap.put("timestamp", str2);
            String str3 = "access_token=" + paramString + "client_id=" + 10230158 + "id=" + paramInt1 + "timestamp=" + str2 + "user_id=" + paramInt2 + "18a75cf12dff8cf6e17550e25c860839";
            Object localObject = "";
            try {
                String str6 = URLEncoder.encode(str3, "utf-8");
                localObject = str6;
            } catch (UnsupportedEncodingException localUnsupportedEncodingException) {
                localUnsupportedEncodingException.printStackTrace();
            }
            String str4 = Constants.get32MD5((String) localObject);
            localHashMap.put("sign", str4);
            ret = HttpTool.post(str1, localHashMap);
        }
        return ret;
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);
        if (bytes != null) {
            String result = new String(bytes, 0, bytes.length);
            if (result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    listener.backResult(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

