package com.qianfeng.android.baoman;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qianfeng.android.baoman.adapter.SubjectFindRecourseAdapter;
import com.qianfeng.android.baoman.model.Subject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

//显示全部专题推荐内容
public class AllSubjectActivity extends Activity implements View.OnClickListener , PullToRefreshBase.OnRefreshListener2 {


    private SubjectFindRecourseAdapter subjectFindRecourseAdapter;
    private PullToRefreshGridView refreshGridView; //allSubjectGridView;
    private List<Subject> specialRecommendRecourse;
    private LinearLayout load_layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_all_subject);

        //返回键
        ImageView backIcon = (ImageView) findViewById(R.id.find_all_special_recommend_top_back);
        backIcon.setOnClickListener(this);

        //网络加载失败提示信息
        load_layout = (LinearLayout) findViewById(R.id.load_layout);

        specialRecommendRecourse = new LinkedList<Subject>();

        //获取PullToRefresh
        refreshGridView = (PullToRefreshGridView) findViewById(R.id.find_recourse_all_special_recommend);

        //设置上拉加载
        refreshGridView.setMode(PullToRefreshBase.Mode.BOTH);

        Drawable drawable = getResources().getDrawable(R.drawable.arrow_down);

        refreshGridView.setLoadingDrawable(drawable);
        // 设置“松开”提醒
        refreshGridView.setReleaseLabel("松开可以刷新");

        // 设置加载中的标题
        refreshGridView.setRefreshingLabel("加载中...");


        //获取内部真实的GridView，进行数据的显示
        GridView allSubjectGridView = refreshGridView.getRefreshableView();

        if (allSubjectGridView != null) {

            subjectFindRecourseAdapter = new SubjectFindRecourseAdapter(this, specialRecommendRecourse);

            allSubjectGridView.setAdapter(subjectFindRecourseAdapter);

            if (specialRecommendRecourse.size() == 0) {
                load_layout.setVisibility(View.VISIBLE);
            }

            HttpUtils httpUtils = new HttpUtils();

            httpUtils.send(HttpRequest.HttpMethod.GET,
                    "http://hahaapi.ibaozou.com/api/v1/subjects.json",
                    new RequestCallBack<String>() {

                        @Override
                        public void onSuccess(ResponseInfo<String> jsonObjectResponseInfo) {
                            String result = jsonObjectResponseInfo.result;
                            List<Subject> subjectList = new LinkedList<Subject>();
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                JSONArray subjectsArray = jsonObject.getJSONArray("subjects");

                                refreshGridView.onRefreshComplete();

                                for (int i = 0; i < subjectsArray.length(); i++) {
                                    JSONObject subjectsArrayJSONObject = subjectsArray.getJSONObject(i);

                                    Log.d("RecourseFindActivity", "--->" + subjectsArrayJSONObject);
                                    Subject subject = new Subject();
                                    subject.parseJSON(subjectsArrayJSONObject);

                                    subjectList.add(subject);
                                }
                                specialRecommendRecourse.addAll(subjectList);
                                subjectFindRecourseAdapter.notifyDataSetChanged();

                                load_layout.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(HttpException e, String s) {
                            Toast.makeText(AllSubjectActivity.this, "连接失败,网络问题", Toast.LENGTH_SHORT).show();

                            load_layout.setVisibility(View.GONE);
                        }
                    });
        }

        //PullToRefreshGridView的刷新事件
        refreshGridView.setOnRefreshListener(this);

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.find_all_special_recommend_top_back:   //返回键
                finish();
                break;
        }
    }


    /////////////上拉加载、下拉刷新的事件处理//////////////////////////////////
    @Override
    public void onPullDownToRefresh(PullToRefreshBase refreshView) {

    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase refreshView) {

    }
}
