package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by Casio on 15-2-4.
 * Email：807705058@qq.com
 */

/**
 * 通用的图片显示Adapter
 */
public class CommonImagePagerAdapter extends PagerAdapter {

    private List<Bitmap> imageIds;
    private Context context;

    public CommonImagePagerAdapter(List<Bitmap> imageIds, Context context) {
        this.imageIds = imageIds;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView ret = new ImageView(context);
        ret.setScaleType(ImageView.ScaleType.FIT_XY);
        ret.setImageBitmap(imageIds.get(position));
        container.addView(ret);
        return ret;
    }

    @Override
    public int getCount() {
        return imageIds.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {

        return view == o;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (object instanceof ImageView) {
            container.removeView((View) object);
        }
    }
}
