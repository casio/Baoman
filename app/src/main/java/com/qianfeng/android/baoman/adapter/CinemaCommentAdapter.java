package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.CinemaComment;
import com.qianfeng.android.baoman.util.URLImageGetter;

import java.util.List;

/**
 * Created by Android_Studio on 2015/3/12.
 * User:Maxiaoyu
 * Date:2015/3/12
 * Email:731436452@qq.com
 */
public class CinemaCommentAdapter extends BaseAdapter {
    private List<CinemaComment> data;
    private LayoutInflater inflater;
    private CinemaComment cinemaComment;
    private View.OnClickListener listener;
    private Context context;
    private URLImageGetter imageGetter;


    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public CinemaCommentAdapter(List<CinemaComment> data, Context context) {
        this.data = data;
        if (context != null) {
            this.context = context;
            inflater = LayoutInflater.from(context);

        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        View ret = null;

        if (convertView == null) {
            ret = inflater.inflate(R.layout.item_cinema_comment_31, parent, false);
        } else {
            ret = convertView;
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.userName = (TextView) ret.findViewById(R.id.item_cinema_comment_3_userName);
            holder.userName.setOnClickListener(listener);
            holder.time = (TextView) ret.findViewById(R.id.item_cinema_comment_3_time);
            holder.content = (TextView) ret.findViewById(R.id.item_cinema_comment_3_content);
            holder.image = (ImageView) ret.findViewById(R.id.item_cinema_comment_3_image);

            holder.layout = (LinearLayout) ret.findViewById(R.id.item_cinema_comment_operator);

            holder.layoutAll = (LinearLayout) ret.findViewById(R.id.item_cinema_comment_sub);

//            holder.ding = (ImageButton) ret.findViewById(R.id.item_cinema_comment_3_ding);
//            holder.ding.setOnClickListener(listener);
//            holder.cai = (ImageButton) ret.findViewById(R.id.item_cinema_comment_3_cai);
//            holder.cai.setOnClickListener(listener);
//            holder.reply = (TextView) ret.findViewById(R.id.item_cinema_comment_3_reply);
//            holder.reply.setOnClickListener(listener);
//            holder.share = (TextView) ret.findViewById(R.id.item_cinema_comment_3_share);
//            holder.share.setOnClickListener(listener);
//            holder.jubao = (TextView) ret.findViewById(R.id.item_cinema_comment_3_jubao);
//            holder.jubao.setOnClickListener(listener);

            ret.setTag(holder);
        }
        cinemaComment = data.get(position);
//        holder.ding.setTag(position);
//        holder.cai.setTag(position);
        holder.userName.setTag(position);
//        holder.jubao.setTag(position);

//        if (cinemaComment.isDing()) {
//            holder.ding.setImageResource(R.drawable.button_has_ding);
//        }
//        if (cinemaComment.isCai()) {
//            holder.cai.setImageResource(R.drawable.button_has_cai);
//        }


        holder.userName.setText(cinemaComment.getUser().getLogin());
        holder.time.setText(cinemaComment.getCreated_at());



        /*TextView content = (TextView)rView.findViewById(R.id.review_content);
        content.setMovementMethod(LinkMovementMethod.getInstance());//加这句才能让里面的超链接生效
        URLImageGetter ReviewImgGetter = new URLImageGetter(MainActivity.this, content);//实例化URLImageGetter类*/
//        content.setText(Html.fromHtml("包含有图片信息的html内容",ReviewImgGetter,null));


        Log.i("CinemaCommentAdapter", cinemaComment.getContent());

        holder.content.setMovementMethod(LinkMovementMethod.getInstance());
        imageGetter = new URLImageGetter(context, holder.content);
        Spanned spanned = Html.fromHtml(cinemaComment.getContent(), imageGetter, null);
        holder.content.setText(spanned);

        final List<CinemaComment> subComments = cinemaComment.getSubComments();


        if(subComments.size()>0){
            if(cinemaComment.isOpen()){
                holder.image.setImageResource(R.drawable.close_children);
            }else{
                holder.image.setImageResource(R.drawable.show_children);
            }
        }
        /*if (!cinemaComment.isFlag()) {
            //内容未展开
            //打开图标
            if (subComments.size() > 0) {
                holder.image.setImageResource(R.drawable.show_children);
            }
//            holder.layout.setVisibility(View.GONE);
        } else {
            //内容展开
            //关闭图标
            if (subComments.size() > 0) {
                holder.image.setImageResource(R.drawable.close_children);
            }
//            holder.layout.setVisibility(View.VISIBLE);
        }*/

        return ret;
    }


    public static class ViewHolder {
        public TextView userName;
        public TextView time;
        public TextView content;
        public ImageView image;

        public LinearLayout layout;

        public LinearLayout layoutAll;


       /* public ImageButton ding;
        public ImageButton cai;
        public TextView reply;
        public TextView share;
        public TextView jubao;*/
    }

}
