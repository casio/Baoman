package com.qianfeng.android.baoman.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.core.BitmapSize;
import com.qianfeng.android.baoman.BitmapHelp;
import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.CinemaSubItem;

import java.util.List;

/**
 * Created by Android_Studio on 2015/3/11.
 * User:Maxiaoyu
 * Date:2015/3/11
 * Email:731436452@qq.com
 */
public class CinemaListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<CinemaSubItem> data;

    public CinemaListAdapter(Context context, List<CinemaSubItem> data) {
        if (context != null) {
            inflater = LayoutInflater.from(context);
        }
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;
        if (convertView != null) {
            ret = convertView;
        } else {
            ret = inflater.inflate(R.layout.item_cinema_list, parent, false);
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.image = (ImageView) ret.findViewById(R.id.item_cinema_list_image);
            holder.title = (TextView) ret.findViewById(R.id.item_cinema_list_title);
            holder.ding = (TextView) ret.findViewById(R.id.item_cinema_list_ding);
            holder.comment = (TextView) ret.findViewById(R.id.item_cinema_list_comment);
            ret.setTag(holder);
        }
        CinemaSubItem item = data.get(position);
        holder.title.setText(item.getTitle());
        holder.ding.setText(Integer.toString(item.getPos()));
        holder.comment.setText(Integer.toString(item.getPublic_comments_count()));
        String imageLink = item.getMp4_image_link();
        if (imageLink != null) {
            BitmapDisplayConfig config = new BitmapDisplayConfig();
//            config.setShowOriginal(true); // 显示原始图片,不压缩, 尽量不要使用,
            // 图片太大时容易OOM。
            config.setBitmapConfig(Bitmap.Config.RGB_565);
            config.setBitmapMaxSize(new BitmapSize(117, 66));
            System.out.println("imageLink = " + imageLink);
            BitmapHelp.getUtils().display(holder.image, imageLink, config);
        }
        return ret;
    }

    public static class ViewHolder {
        public ImageView image;
        public TextView title;
        public TextView ding;
        public TextView comment;
    }
}
