package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Android_Studio on 2015/3/12.
 * User:Maxiaoyu
 * Date:2015/3/12
 * Email:731436452@qq.com
 */
public class Face implements Serializable {
    //    "id":1,"name":"够拽","code":"face1","url":
    private int id;
    private String name;
    private String code;
    private String url;

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                id = jsonObject.getInt("id");
                name = jsonObject.getString("name");
                code = jsonObject.getString("code");
                url = jsonObject.getString("url");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
