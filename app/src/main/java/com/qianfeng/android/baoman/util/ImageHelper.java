package com.qianfeng.android.baoman.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

/**
 * Created by maxiaoyu on 2015/2/7.
 * User:Maxiaoyu
 * Date:2015/2/7
 * Email:731436452@qq.com
 */
public class ImageHelper {

    public static void setImageIcon(String thumb,ImageView imageView){
        setImageIcon(thumb,imageView,0,0);
    }

    public static void setImageIcon(String thumb,ImageView imageView,int reqWidth, int reqHeight){
        if (thumb != null && thumb.length() > 0) {
            ImageCache imageCache = ImageCache.getInstance();
            Bitmap bitmap = imageCache.getImage(thumb);
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            } else {
                //TODO 文件缓存
                FileCache fileCache = FileCache.getInstance();
                byte[] content = fileCache.getContent(thumb);
                if (content != null && content.length > 0) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(content, 0, content.length);
                    imageCache.putImage(thumb, bmp);
                    imageView.setImageBitmap(bmp);
                } else {
                    //TODO 进行网络下载
                    // 每次进行图片下载之前，设置ImageView的Tag，异步任务检查Tag
                    imageView.setTag(thumb);
                    ImageLoader task = new ImageLoader(imageView);
                    task.execute(thumb,Integer.toString(reqWidth),Integer.toString(reqHeight));
                }
            }
        }
    }
}
