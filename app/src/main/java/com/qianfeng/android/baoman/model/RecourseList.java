package com.qianfeng.android.baoman.model;

import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ldy on 2015/3/11.
 */
//发现-资源
public class RecourseList implements Serializable {
    private String pic;//"http://hahabaozou.qiniudn.com/new-titlepics/2014/08/1408332207766-kug22ohg3i5qrk6mw3e79t-bc14cb8055526b7fda14bea0bfbd4100-440.jpg",
    private String name;//"核爆默示录",      //漫画名称
    private String list_type;//"连载中",    //漫画状态
    private String id;//19288,
    private String author_name;//"井上智德",   //作者名称
    private String area;//"日本",
    private String updated_at;//"2014-06-04T23:50:58+08:00",
    private String created_at;//"2014-05-20T02:39:01+08:00",
    private String view_count;//45623,
    private String description;//"2036年日本援过程中她们突出重围呢。
    private String favorited; //false   是否收藏


    private CartoonCategory cartoonCategory;   //漫画的类型
    private CartoonLatestInfo latestInfo;   //漫画的最新消息


    //解析Json数据
    public void parseJSON(JSONObject json) {
        if (json != null) {
            try {
                pic = json.getString("pic");   //漫画图片
                name = json.getString("name");  //漫画名称
                list_type = json.getString("list_type");   //漫画状态
                id = json.getString("id");
                author_name = json.getString("author_name");
                area = json.getString("area");
                updated_at = json.getString("updated_at");
                created_at = json.getString("created_at");
                view_count = json.getString("view_count");
                description = json.getString("description");
                favorited = json.optString("favorited");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getList_type() {
        return list_type;
    }

    public void setList_type(String list_type) {
        this.list_type = list_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getView_count() {
        return view_count;
    }

    public void setView_count(String view_count) {
        this.view_count = view_count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFavorited() {
        return favorited;
    }

    public void setFavorited(String favorited) {
        this.favorited = favorited;
    }

    public CartoonCategory getCartoonCategory() {
        return cartoonCategory;
    }

    public void setCartoonCategory(CartoonCategory cartoonCategory) {
        this.cartoonCategory = cartoonCategory;
    }

    public CartoonLatestInfo getLatestInfo() {
        return latestInfo;
    }

    public void setLatestInfo(CartoonLatestInfo latestInfo) {
        this.latestInfo = latestInfo;
    }
}
