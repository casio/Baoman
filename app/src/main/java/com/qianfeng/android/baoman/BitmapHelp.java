package com.qianfeng.android.baoman;

import android.content.Context;
import android.os.Environment;

import com.lidroid.xutils.BitmapUtils;

import java.io.File;

/**
 * Created by jash on 15-3-7.
 */
public class BitmapHelp {
    private static BitmapUtils utils;
    public static void initUtils(Context context){
        File file = new File(Environment.getExternalStorageDirectory(), "Baoman");
        utils = new BitmapUtils(context,
                file.getAbsolutePath(),
                0.5f);
        utils.configDefaultLoadingImage(R.drawable.top_view_default);
        utils.configDefaultLoadFailedImage(R.drawable.top_view_default);
        utils.configDiskCacheEnabled(true);
//        utils.configDefaultReadTimeout();
//        utils.clearDiskCache();
    }

    public static BitmapUtils getUtils() {
        return utils;
    }
}
