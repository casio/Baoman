package com.qianfeng.android.baoman.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RadioButton;

import com.qianfeng.android.baoman.R;
import com.qianfeng.android.baoman.model.Other;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
        /*RadioButton rb1= (RadioButton) view.findViewById(R.id.raido1);
        rb1.setOnClickListener(this);
        RadioButton rb2= (RadioButton) view.findViewById(R.id.raido2);
        rb2.setOnClickListener(this);
        RadioButton rb3= (RadioButton) view.findViewById(R.id.raido3);
        rb3.setOnClickListener(this);
        RadioButton rb4= (RadioButton) view.findViewById(R.id.raido4);

        RadioButton rb5= (RadioButton) view.findViewById(R.id.raido5);
        RadioButton rb6= (RadioButton) view.findViewById(R.id.raido6);
        RadioButton rb7= (RadioButton) view.findViewById(R.id.raido7);
        RadioButton rb8= (RadioButton) view.findViewById(R.id.raido8);*/

/**
 * Created by Android_Studio on 2015/3/14.
 * User:Maxiaoyu
 * Date:2015/3/14
 * Email:731436452@qq.com
 */
public class ShareDialog implements View.OnClickListener {
    private Context context;

    private Dialog dialog;

    private WindowManager windowManager;

    public ShareDialog(Context context, WindowManager windowManager) {
        this.context = context;
        this.windowManager = windowManager;
    }

    public void showDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_share, null);

        Button button = (Button) view.findViewById(R.id.item_share_cancel);
        button.setOnClickListener(this);


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        dialog = builder.create();
        dialog.show();
        Window window = dialog.getWindow();
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = windowManager.getDefaultDisplay().getWidth();
        window.setAttributes(lp);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.item_share_cancel:
                dialog.dismiss();

                /*Intent intent=new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "分享");
                intent.putExtra(Intent.EXTRA_TEXT, "aaaaaaaaaaa");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                Intent chooser = Intent.createChooser(intent, "qq");
                context.startActivity(chooser);*/
                break;
        }
    }
}