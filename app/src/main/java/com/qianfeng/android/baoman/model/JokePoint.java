package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by zhangkx on 2015/3/10.
 */
public class JokePoint implements Serializable{
    /*
                "id": 2327,
                "title": "事实证明!那里短但是舌功好也是OK的!",
                "description": "<p>事实证明!</p>",
                "icon": "http://wanzao2.b0.upaiyun.com/system/joke_points/icons/2327/original/woshgonghzy.jpg",
    "page": 1,
    "total_count": 4,
    "total_pages": 1,
    "per_page": 20

     */

    private String id;   //"id": 2327,
    private String title;   //"title": "事实证明!那里短但是舌功好也是OK的!",
    private String description;   //"description": "<p>事实证明!</p>",
    private String icon;   //"icon": "http://wanzao2.b0.upaiyun.com/system/joke_points/icons/2327/original/woshgonghzy.jpg",
    private Jokable jokable;



    public void parseJSON(JSONObject json) {
        if (json != null) {
            try {

                id = json.getString("id");
                title = json.getString("title");
                description = json.getString("description");
                icon = json.getString("icon");



                JSONObject jokableJson = json.getJSONObject("jokable");
                jokable = new Jokable();
                jokable.parseJSON(jokableJson);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public Jokable getJokable() {
        return jokable;
    }


}