package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by zhangkx on 2015/3/10.
 */
public class Group implements Serializable {
    private String name;

    public Group() {
    }

    public Group(String name) {
        this.name = name;
    }

    /**
     * 解析JSON数据
     *
     * @param json
     */
    public void parseJSON(JSONObject json) {
        if (json != null) {

            try {
                name = json.getString("name"); // 必须存在的字段优先解析，这样保证数据的有效性

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
