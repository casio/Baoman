package com.qianfeng.android.baoman.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Casio on 15-2-4.
 * Email：807705058@qq.com
 */
public class CommonFragmentAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;


    public CommonFragmentAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment ret = null;
        if (fragments != null) {
            ret = fragments.get(i);
        }
        return ret;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }


}
