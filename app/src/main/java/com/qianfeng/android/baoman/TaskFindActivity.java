package com.qianfeng.android.baoman;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qianfeng.android.baoman.adapter.FindTaskAdapter;
import com.qianfeng.android.baoman.model.Stage;
import com.qianfeng.android.baoman.model.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;


public class TaskFindActivity extends Activity implements View.OnClickListener {

    private FindTaskAdapter adapter;
    private LinkedList<Task> tasks;
    final private String url = "http://api.ibaozou.com/api/v2/tasks?client_id=10230158&user_id=8818558&access_token=1c50067d21cc5ad05d128a1ebba3b2a4fe9887c0&timestamp=1425895776191&sign=1373649f2a0332556781517041a0906e";

    //网络加载失败提示信息
    private LinearLayout load_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_task_find);


        ImageView backIcon = (ImageView) findViewById(R.id.find_task_top_back);
        backIcon.setOnClickListener(this);

        load_layout = (LinearLayout) findViewById(R.id.load_layout);

        tasks = new LinkedList<Task>();

        ListView taskListView = (ListView) findViewById(R.id.find_task_list_view);
        adapter = new FindTaskAdapter(this, tasks);
        if (taskListView != null) {
            taskListView.setAdapter(adapter);
            getHttpData(url);

        }

    }

    private void getHttpData(String url) {

        if (tasks.size() == 0){
            load_layout.setVisibility(View.VISIBLE);
        }

        HttpUtils httpUtils = new HttpUtils();
        httpUtils.send(
                HttpRequest.HttpMethod.GET,
                url,
                new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> jsonObjectResponseInfo) {

                        String result = jsonObjectResponseInfo.result;
                        List<Task> taskList = new LinkedList<Task>();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray tasksArray = jsonObject.getJSONArray("tasks");
                            for (int i = 0; i < tasksArray.length(); i++) {
                                JSONObject tasksArrayJSONObject = tasksArray.getJSONObject(i);
                                JSONObject stageObject = tasksArrayJSONObject.getJSONObject("stage");

                                Stage stage = new Stage();
                                Task task = new Task();

                                stage.parseJSON(stageObject);
                                task.parseJSON(tasksArrayJSONObject);

                                task.setStage(stage);
                                taskList.add(task);
                            }
                            tasks.addAll(taskList);
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        load_layout.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(TaskFindActivity.this, "连接失败，网络问题", Toast.LENGTH_SHORT).show();
                        load_layout.setVisibility(View.GONE);
                    }
                }
        );
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.find_task_top_back:
                finish();
                break;
        }
    }
}