package com.qianfeng.android.baoman;

import android.app.Application;

import com.qianfeng.android.baoman.util.FileCache;

/**
 * Created by Android_Studio on 2015/3/10.
 * User:Maxiaoyu
 * Date:2015/3/10
 * Email:731436452@qq.com
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //初始化文件缓存
        FileCache.createInstance(getApplicationContext());
        Constants.getInstance().setContext(this);
    }


}
