package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Android_Studio on 2015/3/11.
 * User:Maxiaoyu
 * Date:2015/3/11
 * Email:731436452@qq.com
 */
public class CinemaSubItem implements Serializable {
    private int alt_score;// 18144,
    private String comment_status;// "",
    private String content;// "<p><object width=\"480\" height=\"400\" classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0\"><param name=\"src\" value=\"http://player.youku.com/player.php/sid/XOTAzMDg1MTIw/v.swf\" /><param name=\"quality\" value=\"high\" /><param name=\"allowscriptaccess\" value=\"always\" /><param name=\"allowfullscreen\" value=\"true\" /><param name=\"wmode\" value=\"opaque\" /><embed src=\"http://player.youku.com/player.php/sid/XOTAzMDg1MTIw/v.swf\" type=\"application/x-shockwave-flash\" width=\"480\" height=\"400\" wmode=\"transparent\" allowfullscreen=\"true\" quality=\"high\" allowscriptaccess=\"always\" align=\"middle\" /></object></p>",
    private String created_at;// "2015-03-02 15:30:00",
    private String face_id;// "",
    private boolean for_mobile;//"for_mobile": true,
    private int group_id;// 1098,
    private int id;// 16992505,
    private int neg;// -831,
    private String original_id;// "",
    private int pos;// 48444,
    private String published_at;// "2015-03-02 15:42:07",
    private int score;// 47613,
    private int series_id;//
    private String status;// "publish",
    private String title;// "【编辑部的故事59】纸巾也是一个有故事的young man",
    private String updated_at;// "2015-03-11T12:55:29+08:00",
    private int user_id;// 5255257,
    private String pictures;// "http://wanzao2.b0.upaiyun.com/system/pictures/16992505/original/1425282093_460x236.jpg-460",
    private int height;// 236,
    private int width;// 460,
    private String small_pictures;// "http://wanzao2.b0.upaiyun.com/system/avatars/5255257/original/2014092311351889.jpg-s1",
    private Series series;
    private User user;
    /*      "user": {
      "login": "编辑部的故事",
              "avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/5255257/original/2014092311351889.jpg-s1",
              "id": 5255257
  },*/
    private String user_login;// "编辑部的故事",
    private String user_avatar;// "http://wanzao2.b0.upaiyun.com/system/avatars/5255257/original/2014092311351889.jpg-s1",
    private int picture_type;// 3,
    private String mp4_file_link;// "http://hc26.aipai.com/user/596/17601596/4590714/card/24651977/card.mp4",
    private String mp4_image_link;// "http://kindeditor.b0.upaiyun.com/1425281963c0ef49cfd29f230eca89.jpg",
    private int poll_type;// 0,
    //            "poll": {},
    private int reward_count;// 240,
    private int public_comments_count;// 828,
    private int watched_count;// 456,
    private Group group;
    private String ad_url_ios;// "http://v.youku.com/v_show/id_XOTAzMDg1MTIw.html",
    private String ad_url_android;// "http://v.youku.com/v_show/id_XOTAzMDg1MTIw.html",
    private String ad_url_winphone;// "http://v.youku.com/v_show/id_XOTAzMDg1MTIw.html",
    private String ad_url_web;// "http://v.youku.com/v_show/id_XOTAzMDg1MTIw.html"


    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        CinemaSubItem item = null;
        if (o instanceof CinemaSubItem) {
            item = (CinemaSubItem) o;
        }
        return this.title.equals(item.getTitle());
    }

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                alt_score = jsonObject.getInt("alt_score");//72057

                comment_status = jsonObject.optString("comment_status");//"comment_status": "",
                content = jsonObject.getString("content");//"<p><object width=\"480\" height=\"400\" classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0\"><param name=\"src\" value=\"http://player.youku.com/player.php/sid/XOTA4Mzc2NDQ4/v.swf\" /><param name=\"quality\" value=\"high\" /><param name=\"allowscriptaccess\" value=\"always\" /><param name=\"allowfullscreen\" value=\"true\" /><param name=\"wmode\" value=\"opaque\" /><embed src=\"http://player.youku.com/player.php/sid/XOTA4Mzc2NDQ4/v.swf\" type=\"application/x-shockwave-flash\" width=\"480\" height=\"400\" wmode=\"transparent\" allowfullscreen=\"true\" quality=\"high\" allowscriptaccess=\"always\" align=\"middle\" /></object></p>"
                created_at = jsonObject.getString("created_at");//"2015-03-09 19:15:00"

                face_id = jsonObject.optString("face_id");// "face_id": "",
                for_mobile = jsonObject.getBoolean("for_mobile");//"for_mobile": true,

                group_id = jsonObject.getInt("group_id");//1098
                id = jsonObject.getInt("id");//17286500
                neg = jsonObject.getInt("neg");//-411

                original_id = jsonObject.optString("original_id");//"original_id": "",
                pos = jsonObject.getInt("pos");//32651
                published_at = jsonObject.getString("published_at");//"2015-03-09 19:30:00"
                score = jsonObject.getInt("score");//32238
                series_id = jsonObject.getInt("series_id");//1753
                status = jsonObject.getString("status");//"publish"
                title = jsonObject.getString("title");//"【编辑部的故事60】过年对付亲戚逼问的几种有效办法"
                updated_at = jsonObject.getString("updated_at");//"2015-03-11T13:12:50+08:00"
                user_id = jsonObject.getInt("user_id");//5255257
                pictures = jsonObject.optString("pictures");//"http://wanzao2.b0.upaiyun.com/system/pictures/17286500/original/1425900600_460x236.jpg-460"
                height = jsonObject.optInt("height");//236
                width = jsonObject.optInt("width");//460
                small_pictures = jsonObject.getString("small_pictures");//"http://wanzao2.b0.upaiyun.com/system/avatars/5255257/original/2014092311351889.jpg-s1"

                JSONObject jsonSeries = jsonObject.getJSONObject("series");
                series = new Series();
                series.parseJSON(jsonSeries);
                /*    "series": {
                "id": 1753,
                        "name": "编辑部的故事",
                        "icon": "http://wanzao2.b0.upaiyun.com/system/series/icons/1753/original/1753.jpg",
                        "description": "王尼玛和他的编辑部成员们成天在做些什么呢？他们当中到底谁是基佬，谁是蛇精病，谁是正常狗？这些问题在暴走编辑部的故事中，我们也不会告诉你的。",
                        "user_name": "稻草小哪吒",
                        "user_id": 177237
            },*/


                JSONObject jsonUser = jsonObject.getJSONObject("user");
                user = new User();
                user.parseJSON(jsonUser);

         /*   "user": {
                "login": "编辑部的故事",
                        "avatar": "http://wanzao2.b0.upaiyun.com/system/avatars/5255257/original/2014092311351889.jpg-s1",
                        "id": 5255257
            },*/
                user_login = jsonObject.getString("user_login");//"编辑部的故事"
                user_avatar = jsonObject.getString("user_avatar");//"http://wanzao2.b0.upaiyun.com/system/avatars/5255257/original/2014092311351889.jpg-s1"
                picture_type = jsonObject.getInt("picture_type");//3
                mp4_file_link = jsonObject.getString("mp4_file_link");//"http://hc25.aipai.com/user/596/17601596/4590714/card/24727918/card.mp4"
                mp4_image_link = jsonObject.getString("mp4_image_link");//"http://kindeditor.b0.upaiyun.com/1425900545532de0c20238b9257a1e.jpg"
                poll_type = jsonObject.getInt("poll_type");//0
//                    "poll": {},
                reward_count = jsonObject.getInt("reward_count");//60
                public_comments_count = jsonObject.getInt("public_comments_count");//483
                watched_count = jsonObject.getInt("watched_count");//344
                JSONObject jsonGroup = jsonObject.getJSONObject("group");
                group = new Group();
                group.parseJSON(jsonGroup);
            /*        "group": {
                "name": "视频频道"
            },*/
                ad_url_ios = jsonObject.getString("ad_url_ios");//"http://v.youku.com/v_show/id_XOTA4Mzc2NDQ4.html"
                ad_url_android = jsonObject.getString("ad_url_android");//"http://v.youku.com/v_show/id_XOTA4Mzc2NDQ4.html"

                ad_url_web = jsonObject.getString("ad_url_web");   //"ad_url_web": "http://v.youku.com/v_show/id_XOTA4Mzc2NDQ4.html"
                ad_url_winphone = jsonObject.getString("ad_url_winphone");//"http://v.youku.com/v_show/id_XOTA4Mzc2NDQ4.html"
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getAlt_score() {
        return alt_score;
    }

    public void setAlt_score(int alt_score) {
        this.alt_score = alt_score;
    }

    public String getComment_status() {
        return comment_status;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getFace_id() {
        return face_id;
    }

    public void setFace_id(String face_id) {
        this.face_id = face_id;
    }

    public boolean isFor_mobile() {
        return for_mobile;
    }

    public void setFor_mobile(boolean for_mobile) {
        this.for_mobile = for_mobile;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNeg() {
        return neg;
    }

    public void setNeg(int neg) {
        this.neg = neg;
    }

    public String getOriginal_id() {
        return original_id;
    }

    public void setOriginal_id(String original_id) {
        this.original_id = original_id;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getPublished_at() {
        return published_at;
    }

    public void setPublished_at(String published_at) {
        this.published_at = published_at;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getSeries_id() {
        return series_id;
    }

    public void setSeries_id(int series_id) {
        this.series_id = series_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getSmall_pictures() {
        return small_pictures;
    }

    public void setSmall_pictures(String small_pictures) {
        this.small_pictures = small_pictures;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public int getPicture_type() {
        return picture_type;
    }

    public void setPicture_type(int picture_type) {
        this.picture_type = picture_type;
    }

    public String getMp4_file_link() {
        return mp4_file_link;
    }

    public void setMp4_file_link(String mp4_file_link) {
        this.mp4_file_link = mp4_file_link;
    }

    public String getMp4_image_link() {
        return mp4_image_link;
    }

    public void setMp4_image_link(String mp4_image_link) {
        this.mp4_image_link = mp4_image_link;
    }

    public int getPoll_type() {
        return poll_type;
    }

    public void setPoll_type(int poll_type) {
        this.poll_type = poll_type;
    }

    public int getReward_count() {
        return reward_count;
    }

    public void setReward_count(int reward_count) {
        this.reward_count = reward_count;
    }

    public int getPublic_comments_count() {
        return public_comments_count;
    }

    public void setPublic_comments_count(int public_comments_count) {
        this.public_comments_count = public_comments_count;
    }

    public int getWatched_count() {
        return watched_count;
    }

    public void setWatched_count(int watched_count) {
        this.watched_count = watched_count;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getAd_url_ios() {
        return ad_url_ios;
    }

    public void setAd_url_ios(String ad_url_ios) {
        this.ad_url_ios = ad_url_ios;
    }

    public String getAd_url_android() {
        return ad_url_android;
    }

    public void setAd_url_android(String ad_url_android) {
        this.ad_url_android = ad_url_android;
    }

    public String getAd_url_winphone() {
        return ad_url_winphone;
    }

    public void setAd_url_winphone(String ad_url_winphone) {
        this.ad_url_winphone = ad_url_winphone;
    }

    public String getAd_url_web() {
        return ad_url_web;
    }

    public void setAd_url_web(String ad_url_web) {
        this.ad_url_web = ad_url_web;
    }
}
