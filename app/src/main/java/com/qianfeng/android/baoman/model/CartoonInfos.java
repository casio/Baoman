package com.qianfeng.android.baoman.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ldy on 2015/3/16.
 */
public class CartoonInfos {
    private String id;//544621,
    private String name;//"第01话",
    private String download_url;//"http://hahazip.qiniudn.com/544621a888871268888a12696.zip"

    public void parseJson(JSONObject json){
        try {
            id = json.getString("id");
            name = json.getString("name");
            download_url = json.getString("download_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownload_url() {
        return download_url;
    }

    public void setDownload_url(String download_url) {
        this.download_url = download_url;
    }
}
