package com.qianfeng.android.baoman.util;

import android.text.TextUtils;

import com.qianfeng.android.baoman.Constants;
import com.qianfeng.android.baoman.model.LoginUser;

/**
 * Created by Android_Studio on 2015/3/17.
 * User:Maxiaoyu
 * Date:2015/3/17
 * Email:731436452@qq.com
 */
public class LoginUtils {
    private LoginUtils() {
    }

    public static LoginUser getLoginUser() {
        LoginUser loginUser = null;
        Constants instance = Constants.getInstance();
        String time = instance.load("lastTime");
        if (TextUtils.isEmpty(time)) {
            return loginUser;
        }
        Long lastTime = Long.parseLong(time);
        long currentTime = System.currentTimeMillis();
        //设置两天过期
        long staleDated = 10 * 60 * 1000;
        if ((currentTime - lastTime) > staleDated) {
            return loginUser;
        }
        loginUser = instance.getUser();
        return loginUser;
    }
}
